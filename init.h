#ifndef _INIT_H
#define _INIT_H

#include <iostream>
#include <fstream>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdarg.h>
#include <string>

#include <SDL/SDL.h>
#include <gl/glut.h>

/// Raczej jasne... Wlasciwosc ekranu.
#define SCREEN_WIDTH  1024
/// j.w.
#define SCREEN_HEIGHT 768
/// j.w.
#define SCREEN_DEPTH  8
/// j.w.

// Rozmiar bufora potrzebnego do odnajdywania obiektow pod myszka.
#define BUFSIZE       512

////////////////////////////////////////////////////////////////////////////////
using namespace std;
////////////////////////////////////////////////////////////////////////////////
	
extern SDL_Surface *mainWindow;
extern bool bLight;
extern int theObject;
extern fstream myLog;
extern bool altGLuint, noTexturing;
extern bool bDone;
extern bool bOrtho;
extern int videoFlags;
////////////////////////////////////////////////////////////////////////////////
/// Klasa opisujaca wektory trojwymiarowe. 
////////////////////////////////////////////////////////////////////////////////
struct cVector3f {
	public:
		cVector3f(){};
		cVector3f( float X, float Y, float Z ) {
			x = X;
			y = Y;
			z = Z;
		}
		
		
		/// Przeladowania operatorow.
		cVector3f operator+( cVector3f vVector ) {
            return cVector3f(vVector.x + x, vVector.y + y, vVector.z + z);
        } 
        cVector3f operator-( cVector3f vVector ) {
            return cVector3f(x - vVector.x, y - vVector.y, z - vVector.z);
        }
        cVector3f operator*( float num ) {
            return cVector3f(x * num, y * num, z * num);
        }
        cVector3f operator*( int num ) {
            return cVector3f(x * num, y * num, z * num);
        }
        cVector3f operator/( float num ) {
            return cVector3f(x / num, y / num, z / num);
        }    
		cVector3f & operator=( const cVector3f &vVector ) {
		    x = vVector.x; 
            y = vVector.y; 
            z = vVector.z;    
		    return *this; 
		} 
		
		/// Wspolrzedne wektora.
		float x, y, z;
}; // cVector3f
	
/// Struktura opisujaca kolor w systemie RGB.
struct cColor {
	cColor(){};
	cColor( float Xr, float Xg, float Xb ) {
		r = Xr;
		g = Xg;
		b = Xb;
	} 
    float r; /// red
	float g; /// green
	float b; /// blue
};
////////////////////////////////////////////////////////////////////////////////
// Obsluga trybow graficznych.
void toggleFullScreen( void );
void start( void );
void sizeOpenGLScreen( int, int );
void quit( int ret_val );
void startPicking( int MouseX, int MouseY );
void stopPicking( void );
void orthoBegin( void );
void orthoEnd( void );

// Funkcje zamieniajace.
void swap( unsigned char &a, unsigned char &b );
void swap( int &a, int &b );
void swap( float &a, float &b );

// Wypisywanie na ekran.
void write( float x, float y, char *string );

#endif // _INIT_H

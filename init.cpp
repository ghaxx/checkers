#include "init.h"
#include "camera.h"
#include "drawings.h"
#include "textures.h"
#include "peripherals.h"
#include "menu.h"

/// Tablica obiektow bedacych aktualnie pod wskaznikiem myszy.
GLuint selectBuf[BUFSIZE];

/// ID obiektu pod mysza i najblizszego kamerze. ( -1 -> pod mysza nic nie ma )
int theObject =  - 1;

/// Zmienne niezbedne przy szukaniu obiektu pod musza najblizszego kamerze.
GLuint minZ, minB, hits;

/// Stosunek wysokosci okna do szerokosci.
float ratio;

/// Atrybuty tworzonego okna.
int videoFlags = 0;

/// Wskaznik do tworzonego okna.
SDL_Surface *mainWindow = NULL;

/// Wlacznik swiatla.
bool bLight = true;

/// ...
fstream myLog( "log.txt", ios::out | ios::trunc );

/// Domyslne wlasciwosci zmiennych zaleznych od parametrow podawanych przy uruchamianiu.
bool altGLuint = false, noTexturing = false;

/// False, kiedy wyswietlana jest scena w pelnym 3D; true, kiedy pracuje w trybie 2D.
bool bOrtho = false;

/// Czy zakonczono aplikacje... false - jeszcze nie zakonczono.
bool bDone = false;
////////////////////////////////////////////////////////////////////////////////
/// Kilka funkcji zamieniajacych zmienne miejscami.
////////////////////////////////////////////////////////////////////////////////
void swap( unsigned char &a, unsigned char &b ) {
	unsigned char temp;
	temp = a;
	a = b;
	b = temp;
	return ;
}
void swap( int &a, int &b ) {
	int temp;
	temp = a;
	a = b;
	b = temp;
	return ;
}
void swap( float &a, float &b ) {
	float temp;
	temp = a;
	a = b;
	b = temp;
	return ;
}
/// Przechodzenie w tryb pelnoekranowy. 
void toggleFullScreen( void ) {
	if ( SDL_WM_ToggleFullScreen( mainWindow ) == 0 ) {
		cerr << "Nieudana inicjalizacja trybu pelnoekranowego: " << SDL_GetError() << endl;
		quit( 0 );
	}
}
/// Tworzenie okna aplikacji. 
void createMyWindow( const char *strWindowName, int width, int height, int VideoFlags ) {
	mainWindow = SDL_SetVideoMode( width, height, SCREEN_DEPTH, VideoFlags );
	if ( mainWindow == NULL ) {
		cerr << "Failed to Create Window : " << SDL_GetError() << endl;
		quit( 0 );
	}
	videoFlags = VideoFlags;
	SDL_WM_SetCaption( strWindowName, strWindowName );
}
////////////////////////////////////////////////////////////////////////////////
/// Ustawianie trybu graficznego. 
////////////////////////////////////////////////////////////////////////////////
void setupPixelFormat( void ) {
	videoFlags = SDL_OPENGL;
	videoFlags |= SDL_HWPALETTE;
	// Nie dziala
	//videoFlags   |= SDL_RESIZABLE;
	const SDL_VideoInfo *videoInfo = SDL_GetVideoInfo();
	if ( videoInfo == NULL ) {
		cerr << "Failed getting Video Info : " << SDL_GetError() << endl;
	}

	if ( videoInfo->hw_available ) {
		videoFlags |= SDL_HWSURFACE;
	} else {
		videoFlags |= SDL_SWSURFACE;
	}

	if ( videoInfo->blit_hw ) {
		videoFlags |= SDL_HWACCEL;
	}

	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, SCREEN_DEPTH );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_ACCUM_RED_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_ACCUM_GREEN_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_ACCUM_BLUE_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_ACCUM_ALPHA_SIZE, 0 );
}
////////////////////////////////////////////////////////////////////////////////
/// Inicjalizowanie okna aplikacji. 
////////////////////////////////////////////////////////////////////////////////
void sizeOpenGLScreen( int width, int height ) {
	if ( height == 0 ) {
		height = 1;
	}
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective( 45.0f, ( GLfloat )width / ( GLfloat )height, 0.01f, 1000.0f );
	ratio = ( GLfloat )width / ( GLfloat )height;
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
}
////////////////////////////////////////////////////////////////////////////////
/// Uruchamiane trybu OpenGL. 
////////////////////////////////////////////////////////////////////////////////
void initializeOpenGL( int width, int height ) {
	//    float fogColor[4] = {0.1f,0.1f,0.4f,1.0f};
	//    float g_FogDensity=0.1f;

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_LIGHTING );
	glEnable( GL_COLOR_MATERIAL );
	glShadeModel( GL_SMOOTH );
	//glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	glClearColor( 0.0, 0.0, 0.0, 1 );
	glDepthFunc( GL_LEQUAL );
	glClearDepth( 1.0f );

	// Mgla... Spowalnia i ogolnie nic poza tym...
	//    glFogi(GL_FOG_MODE, GL_EXP2);
	//    glFogfv(GL_FOG_COLOR, fogColor);
	//    glFogf(GL_FOG_DENSITY, g_FogDensity);
	//    glHint(GL_FOG_HINT, GL_NICEST);
	//    glFogf(GL_FOG_START, 0.0f);
	//    glFogf(GL_FOG_END, 1000.0f);
	//    glEnable(GL_FOG);

	//    GL_EXP2 GL_EXP GL_LINEAR.
	//    GL_NICEST GL_FASTEST GL_DONT_CARE

	sizeOpenGLScreen( width, height );
}
////////////////////////////////////////////////////////////////////////////////
/// Inicjalizacja kamery, szachownicy i menu.
////////////////////////////////////////////////////////////////////////////////
void init() {
	initializeOpenGL( SCREEN_WIDTH, SCREEN_HEIGHT );
	srand(( unsigned )time( NULL ));
	myCamera.positionCamera( 3, 5, 14, 3, 5, 0, 0, 1, 0 );
	myChessboard.initBoard();
	myGUI.init();
}
////////////////////////////////////////////////////////////////////////////////
/// Wszystkie procedury inicjujace w jednej, zgrabnej formule. 
////////////////////////////////////////////////////////////////////////////////
void start(void) {
    setupPixelFormat();
	createMyWindow( "Warcaby", SCREEN_WIDTH, SCREEN_HEIGHT, videoFlags );
	init();
}
////////////////////////////////////////////////////////////////////////////////
/// Funkcja odpowiedzialna za poprawne zakonczenie aplikacji. 
////////////////////////////////////////////////////////////////////////////////
void quit( int ret_val ) {
	SDL_Quit();
	exit( ret_val );
}
////////////////////////////////////////////////////////////////////////////////
/// Funkcja rozpoczynajaca tryb poszukiwania obiektow pod mysza. 
////////////////////////////////////////////////////////////////////////////////

/// Przyjmuje aktualna pozycje myszy i zwraca tablice elementow znajdujacych sie
/// pod mysza. Aby to zrobic, przelacza tryb wyswietlania grafiki na krotka
/// chwile z MODELVIEW (zwykla projekcja obiektow) na SELECT.
void startPicking( int MouseX, int MouseY ) {
	GLint viewport[4];
	glSelectBuffer( BUFSIZE, selectBuf );
	glRenderMode( GL_SELECT );
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	glGetIntegerv( GL_VIEWPORT, viewport );
	gluPickMatrix( MouseX, viewport[3] - MouseY, 5, 5, viewport );
	gluPerspective( 45, ratio, 0.1, 1000 );
	glMatrixMode( GL_MODELVIEW );
	glInitNames();
}
////////////////////////////////////////////////////////////////////////////////
/// Funkcja szuka najblizszego nam elementu. 
////////////////////////////////////////////////////////////////////////////////

/// Przyjmuje tablice wygenerowana przez funkcje startPicking( int MouseX, int MouseY )
/// i zwraca do zmiennej globalnej theObject ID najblizszego elementu z tejze tablicy.
///
/// Zaleznie od zmiennej altGLuint, ustawianej przez parametr zadawany przy uruchamianiu
/// (true, kiedy uzyto parametru), roznie interpretowane sa zmienne typu GLuint.
/// GLuint to unsigned int, ale czasem potrafi byc ujemny. Przynajmniej na niektorych
/// kartach grafiki...
void processHits( GLint hits, GLuint buffer[] ) {
	unsigned int i, j, newObject;
	GLuint names,  *ptr,  *ptrNames, numberOfNames;
	ptr = ( GLuint* )buffer;
	if ( altGLuint ) {
		minZ = 0xffffffff;
		minB = 0xffffffff;
		for ( i = 0; i < hits; i++ ) {
			names =  *ptr;
			if ( *ptr > 0 ) {
				ptr += 2;
				if ( *ptr < minZ ) {
					numberOfNames = names;
					minZ =  *ptr;
					ptrNames = ptr + 1;
				}
				ptr += names + 1;
			} else {
				ptr += 1;
				if ( *ptr < minB ) {
					minB =  *ptr;
				}
				ptr += 2;
			}
		}
		if ( minB < minZ ) {
			theObject =  *ptrNames;
		} else {
			theObject =  - 1;
		}
	} else {
		minZ = 0;
		minB = 0; 
		for ( i = 0; i < hits; i++ ) {
			names =  *ptr;
			if ( *ptr > 0 ) {
				ptr += 2;
				if ( *ptr > minZ ) {
					numberOfNames = names;
					minZ =  *ptr;
					ptrNames = ptr + 1;
				}
				ptr += names + 1;
			} else {
				ptr += 1;
				if ( *ptr > minB ) {
					minB =  *ptr;
				}
				ptr += 2;
			}
		}
		if ( minB > minZ ) {
			theObject =  *ptrNames;
		} else {
			theObject =  - 1;
		}
	}

}
////////////////////////////////////////////////////////////////////////////////
/// Konczy tryb poszukiwania obiektu pod mysza. 
////////////////////////////////////////////////////////////////////////////////

/// Konczy tryb SELECT i przywraca MODELVIEW.
/// Jesli pod mysza sa w ogole jakies obiekty to wywoluje funkcje processHits, 
/// ktora wyszukuje ten najblizszy.
void stopPicking() {
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glFlush();
	hits = glRenderMode( GL_RENDER );
	if ( hits != 0 ) {
		processHits( hits, selectBuf );
	}
}
////////////////////////////////////////////////////////////////////////////////
/// Uruchamianie trybu 2D. 
////////////////////////////////////////////////////////////////////////////////

/// Wlasciwie jest to tryb pseudo-2D, gdyz po prostu zyskujemy mozliwosc
/// odwolywania sie do kolejnych pixeli sceny, podajac ich wspolzedne wzgledem okna
/// aplikacji, gdzie (0,0) to lewy gorny rog, a (SCREEN_WIDTH, SCREEN_HEIGHT) - 
/// - prawy dolny.
void orthoBegin() {
	bOrtho = true;
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D( 0, SCREEN_WIDTH, 0, SCREEN_HEIGHT );
	glScalef( 1,  - 1, 1 );
	glTranslatef( 0,  - SCREEN_HEIGHT, 0 );
	glMatrixMode( GL_MODELVIEW );
}
////////////////////////////////////////////////////////////////////////////////
/// Powrot do pelnego 3D. 
////////////////////////////////////////////////////////////////////////////////
void orthoEnd() {
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
	bOrtho = false;
}
////////////////////////////////////////////////////////////////////////////////
/// Wypisywanie tekstu na ekran. 
////////////////////////////////////////////////////////////////////////////////

/// Wypisuje tekst string umieszczajac jego poczatek na wspolzednych (x, y) [tryb 2D 
/// wymagany, aby zadzialalo]. Miedzy literami zostawia (spacing) pixeli przerwy.
void renderSpacedBitmapString( float x, float y, int spacing, void *font, char *string ) {
	char *c;
	int i = 0;
	int x1 = int( x );
	for ( c = string;  *c != '\0'; c++ ) {
		glRasterPos2f( x1, y );
		glutBitmapCharacter( font,  *c );
		x1 = x1 + glutBitmapWidth( font,  *c ) + spacing;
		i++;
	}

}
////////////////////////////////////////////////////////////////////////////////
/// Usprawnienienie wypisywania na ekran. 
////////////////////////////////////////////////////////////////////////////////

/// Poniewaz mozna pisac na ekranie tylko w trybie 2D, ta funkcja zabezpiecza,
/// aby rzeczywiscie nastepowalo to w trybie 2D.
void write( float x, float y, char *string ) {
    if(!bOrtho){
        orthoBegin();
        renderSpacedBitmapString( x, y, 0, GLUT_BITMAP_9_BY_15, string );
        orthoEnd();
    } else {
        renderSpacedBitmapString( x, y, 0, GLUT_BITMAP_9_BY_15, string );
    }   
}


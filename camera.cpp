#include "init.h"
#include "camera.h"
#include "drawings.h"
#include "textures.h"
#include "peripherals.h"
#include "menu.h"

cCamera myCamera;
float kSpeed = 0.1;
//==============================================================================
cVector3f cross( cVector3f vVector1, cVector3f vVector2 ) {
	cVector3f vNormal;

	vNormal.x = (( vVector1.y *vVector2.z ) - ( vVector1.z *vVector2.y ));
	vNormal.y = (( vVector1.z *vVector2.x ) - ( vVector1.x *vVector2.z ));
	vNormal.z = (( vVector1.x *vVector2.y ) - ( vVector1.y *vVector2.x ));

	return vNormal;
}

//==============================================================================
float magnitude( cVector3f vNormal ) {
	return ( float )sqrt(( vNormal.x *vNormal.x ) + ( vNormal.y *vNormal.y ) + ( vNormal.z *vNormal.z ));
}

//==============================================================================
cVector3f normalize( cVector3f vVector ) {
	float mag = magnitude( vVector );
	vVector = vVector / mag;
	return vVector;
}

//==============================================================================
cCamera::cCamera() {
	cVector3f vZero = cVector3f( 0.0, 0.0, 0.0 );
	cVector3f vView = cVector3f( 0.0, 1.0, 0.5 );
	cVector3f vUp = cVector3f( 0.0, 0.0, 1.0 );

	m_vPosition = vZero;
	m_vView = vView;
	m_vUpVector = vUp;
}

//==============================================================================
void cCamera::positionCamera( float positionX, float positionY, float positionZ, float viewX, float viewY, float viewZ, float upVectorX, float upVectorY, float upVectorZ ) {

	cVector3f vPosition( positionX, positionY, positionZ );
	cVector3f vView( viewX, viewY, viewZ );
	cVector3f vUpVector( upVectorX, upVectorY, upVectorZ );

	m_vPosition = vPosition;
	m_vView = vView;
	m_vUpVector = vUpVector;
}

//==============================================================================
void cCamera::moveCamera( float speed ) {
	cVector3f vVector( 0, 0, 0 );
	// get view vector (the direciton faced)
	vVector = m_vView - m_vPosition;
	vVector = normalize( vVector );
	m_vPosition = m_vPosition + ( vVector * speed );
	m_vView = m_vView + ( vVector * speed );
}

//==============================================================================
void cCamera::moveCameraFlat( float speed ) {
	cVector3f vVector( 0, 0, 0 );

	vVector = m_vView - m_vPosition;
	vVector = normalize( vVector );
	m_vPosition.x += vVector.x * speed;
	m_vPosition.z += vVector.z * speed;
	m_vView.x += vVector.x * speed;
	m_vView.z += vVector.z * speed;
}

//==============================================================================
void cCamera::verticalMove( float speed ) {
	//    cVector3f vVector(0,0,0);
	//
	//    vVector = m_vView - m_vPosition;
	//    vVector = normalize(vVector);
	m_vPosition.y += speed;
	m_vView.y += speed;
}

//==============================================================================
void cCamera::strafeCamera( float speed ) {
	m_vPosition.x += m_vStrafe.x * speed;
	m_vPosition.z += m_vStrafe.z * speed;
	m_vView.x += m_vStrafe.x * speed;
	m_vView.z += m_vStrafe.z * speed;
}

//==============================================================================
void cCamera::rotateView( float angle, float x, float y, float z ) {
	cVector3f vNewView;
	cVector3f vView;

	vView.x = m_vView.x - m_vPosition.x;
	vView.y = m_vView.y - m_vPosition.y;
	vView.z = m_vView.z - m_vPosition.z;

	float cosTheta = ( float )cos( angle );
	float sinTheta = ( float )sin( angle );

	vNewView.x = ( cosTheta + ( 1-cosTheta ) *x * x ) *vView.x;
	vNewView.x += (( 1-cosTheta ) *x * y - z * sinTheta ) *vView.y;
	vNewView.x += (( 1-cosTheta ) *x * z + y * sinTheta ) *vView.z;

	vNewView.y = (( 1-cosTheta ) *x * y + z * sinTheta ) *vView.x;
	vNewView.y += ( cosTheta + ( 1-cosTheta ) *y * y ) *vView.y;
	vNewView.y += (( 1-cosTheta ) *y * z - x * sinTheta ) *vView.z;

	vNewView.z = (( 1-cosTheta ) *x * z - y * sinTheta ) *vView.x;
	vNewView.z += (( 1-cosTheta ) *y * z + x * sinTheta ) *vView.y;
	vNewView.z += ( cosTheta + ( 1-cosTheta ) *z * z ) *vView.z;


	m_vView.x = m_vPosition.x + vNewView.x;
	m_vView.y = m_vPosition.y + vNewView.y;
	m_vView.z = m_vPosition.z + vNewView.z;
}

//==============================================================================
void cCamera::rotateAroundPoint( cVector3f vCenter, float angle, float x, float y, float z ) {
	cVector3f vNewPosition;
	cVector3f vPos = m_vPosition - vCenter;

	float cosTheta = ( float )cos( angle );
	float sinTheta = ( float )sin( angle );

	vNewPosition.x = ( cosTheta + ( 1-cosTheta ) *x * x ) *vPos.x;
	vNewPosition.x += (( 1-cosTheta ) *x * y - z * sinTheta ) *vPos.y;
	vNewPosition.x += (( 1-cosTheta ) *x * z + y * sinTheta ) *vPos.z;

	vNewPosition.y = (( 1-cosTheta ) *x * y + z * sinTheta ) *vPos.x;
	vNewPosition.y += ( cosTheta + ( 1-cosTheta ) *y * y ) *vPos.y;
	vNewPosition.y += (( 1-cosTheta ) *y * z - x * sinTheta ) *vPos.z;

	vNewPosition.z = (( 1-cosTheta ) *x * z - y * sinTheta ) *vPos.x;
	vNewPosition.z += (( 1-cosTheta ) *y * z + x * sinTheta ) *vPos.y;
	vNewPosition.z += ( cosTheta + ( 1-cosTheta ) *z * z ) *vPos.z;

	m_vPosition = vCenter + vNewPosition;
}
//==============================================================================
void cCamera::rotateAroundChessboard( int mouseX, int mouseY ) {
	cVector3f vCenter = cVector3f(myChessboard.x, myChessboard.y, myChessboard.z );
		
	int currentX, currentY;
	float angleY = 0.0f;
	float angleZ = 0.0f;
	static float currentRotX = 0.0f;
	
	SDL_GetMouseState( &currentX, &currentY );

	if (( currentX == mouseX ) && ( currentY == mouseY )) {
		return ;
	} angleY = ( float )(( mouseX - currentX )) / 200.0f;
	angleZ = ( float )(( mouseY - currentY )) / 200.0f;
	mouseX = currentX;
	mouseY = currentY;

	currentRotX -= angleZ;

	if ( currentRotX > 1.5f ) {
		currentRotX = 1.5f;
	} else if ( currentRotX <  - 1.5f ) {
		currentRotX =  - 1.5f;
	} else {
		cVector3f vAxis = cross( m_vView - m_vPosition, m_vUpVector );
		vAxis = normalize( vAxis );

		rotateAroundPoint( vCenter, angleZ, vAxis.x, vAxis.y, vAxis.z );
		rotateAroundPoint( vCenter, angleY, 0, 1, 0 );
	}
}
//==============================================================================
void cCamera::setViewByMouse( int mouseX, int mouseY ) {
	int currentX, currentY;
	float angleY = 0.0f;
	float angleZ = 0.0f;
	static float currentRotX = 0.0f;

	SDL_GetMouseState( &currentX, &currentY );

	if (( currentX == mouseX ) && ( currentY == mouseY )) {
		return ;
	} angleY = ( float )(( mouseX - currentX )) / 300.0f;
	angleZ = ( float )(( mouseY - currentY )) / 300.0f;
	mouseX = currentX;
	mouseY = currentY;

	currentRotX -= angleZ;

	if ( currentRotX > 1.5f ) {
		currentRotX = 1.5f;
	} else if ( currentRotX <  - 1.5f ) {
		currentRotX =  - 1.5f;
	} else {
		cVector3f vAxis = cross( m_vView - m_vPosition, m_vUpVector );
		vAxis = normalize( vAxis );

		rotateView( angleZ, vAxis.x, vAxis.y, vAxis.z );
		rotateView( angleY, 0, 1, 0 );
	}
}

//==============================================================================
void cCamera::rotateAroundViewByMouse( int mouseX, int mouseY ) {
	int currentX, currentY;
	float angleY = 0.0f;
	float angleZ = 0.0f;
	static float currentRotX = 0.0f;

	SDL_GetMouseState( &currentX, &currentY );

	if (( currentX == mouseX ) && ( currentY == mouseY )) {
		return ;
	} angleY = ( float )(( mouseX - currentX )) / 200.0f;
	angleZ = ( float )(( mouseY - currentY )) / 200.0f;
	mouseX = currentX;
	mouseY = currentY;

	currentRotX -= angleZ;

	if ( currentRotX > 1.5f ) {
		currentRotX = 1.5f;
	} else if ( currentRotX <  - 1.5f ) {
		currentRotX =  - 1.5f;
	} else {
		cVector3f vAxis = cross( m_vView - m_vPosition, m_vUpVector );
		vAxis = normalize( vAxis );

		rotateAroundPoint( m_vView, angleZ, vAxis.x, vAxis.y, vAxis.z );
		rotateAroundPoint( m_vView, angleY, 0, 1, 0 );
	}
}

//==============================================================================
void cCamera::setViewByMouseNWarp( int mouseX, int mouseY ) {
	int currentX, currentY;
	int middleX = SCREEN_WIDTH >> 1;
	int middleY = SCREEN_HEIGHT >> 1;
	float angleY = 0.0f;
	float angleZ = 0.0f;
	static float currentRotX = 0.0f;

	//    if(firstMove == 1){
	SDL_WarpMouse( middleX, middleY );
	//    }else{
	SDL_GetMouseState( &currentX, &currentY );
	if (( currentX == middleX ) && ( currentY == middleY )) {
		return ;
	} angleY = ( float )(( middleX - currentX )) / 300.0f;
	angleZ = ( float )(( middleY - currentY )) / 300.0f;

	if (( currentX == mouseX ) && ( currentY == mouseY )) {
		return ;
	}

	currentRotX -= angleZ;

	if ( currentRotX > 1.4f ) {
		currentRotX = 1.4f;
	} else if ( currentRotX <  - 1.4f ) {
		currentRotX =  - 1.4f;
	}
	//   else
	 {
		cVector3f vAxis = cross( m_vView - m_vPosition, m_vUpVector );
		vAxis = normalize( vAxis );

		rotateView( angleZ, vAxis.x, vAxis.y, vAxis.z );
		rotateView( angleY, 0, 1, 0 );
	}
	//    } // if(firstMove...
}

//==============================================================================
void cCamera::checkForMovement() {
	// moving flat
	if ( myKeyboard.upPressed ) {
		moveCameraFlat( kSpeed );
	}
	if ( myKeyboard.downPressed ) {
		moveCameraFlat(  - kSpeed );
	}
	if ( myKeyboard.leftPressed ) {
		rotateView( kSpeed, 0, 1, 0 );
	}
	if ( myKeyboard.rightPressed ) {
		rotateView(  - kSpeed, 0, 1, 0 );
	}
	// moving 3D
	if ( myKeyboard.wPressed ) {
		moveCamera( kSpeed );
	}
	if ( myKeyboard.sPressed ) {
		moveCamera(  - kSpeed );
	}
	if ( myKeyboard.aPressed ) {
		strafeCamera(  - kSpeed );
	}
	if ( myKeyboard.dPressed ) {
		strafeCamera( kSpeed );
	}
	// vertical moves
	if ( myKeyboard.qPressed ) {
		verticalMove( kSpeed );
	}
	if ( myKeyboard.ePressed ) {
		verticalMove(  - kSpeed );
	}
	// mouse
	if ( myMouse.rightDown ) {
		setViewByMouse( myMouse.getX(), myMouse.getY() );
	}
	if ( myMouse.middleDown ) {
		rotateAroundChessboard( myMouse.getX(), myMouse.getY() );
	}
}

//==============================================================================
void cCamera::update() {
	cVector3f vCross = cross( m_vView - m_vPosition, m_vUpVector );
	m_vStrafe = normalize( vCross );
	checkForMovement();
}

//==============================================================================
void cCamera::look() {
	gluLookAt( m_vPosition.x, m_vPosition.y, m_vPosition.z, m_vView.x, m_vView.y, m_vView.z, m_vUpVector.x, m_vUpVector.y, m_vUpVector.z );
}

//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================
//==============================================================================

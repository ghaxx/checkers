#include "init.h"
#include "drawings.h"
#include "textures.h"
#include "peripherals.h"

unsigned int textureList[MAX_TEXTURES];
//==============================================================================
void createTexture( unsigned int textureArray[], char *strFileName, int textureID ) {
	SDL_Surface *pBitmap[1];
	if ( strFileName == NULL ) {
		return ;
	}
	pBitmap[0] = SDL_LoadBMP( strFileName );
	if ( pBitmap[0] == NULL ) {
		cerr << " Failed loading " << strFileName << " : " << SDL_GetError() << endl;
		quit( 0 );
	}
	cout << textureID << " before:" << textureArray[textureID] << "\n";
	glGenTextures( 1, &textureArray[textureID] );
	cout << textureID << " after:" << textureArray[textureID] << "\n";
	glBindTexture( GL_TEXTURE_2D, textureArray[textureID] );

	int width = pBitmap[0]->w;
	int height = pBitmap[0]->h;
	unsigned char *data = ( unsigned char* )( pBitmap[0]->pixels ); // the pixel data
	unsigned char *newData = new unsigned char[width *height * 3];
	int channels = 3; // R,G,B
	int BytesPerPixel = pBitmap[0]->format->BytesPerPixel;
	for ( int i = 0; i < ( height / 2 ); ++i )
		for ( int j = 0; j < width *BytesPerPixel; j += BytesPerPixel )
	for ( int k = 0; k < BytesPerPixel; ++k ) {
		swap( data[( i *width * BytesPerPixel ) + j + k], data[(( height - i - 1 ) *width * BytesPerPixel ) + j + k] );
	}
	for ( int i = 0; i < ( width *height ); ++i ) {
		byte r, g, b;
		Uint32 pixel_value = 0;
		for ( int j = BytesPerPixel - 1; j >= 0; --j ) {
			pixel_value = pixel_value << 8;
			pixel_value = pixel_value | data[( i *BytesPerPixel ) + j];
		}

		SDL_GetRGB( pixel_value, pBitmap[0]->format, ( Uint8* ) &r, ( Uint8* ) &g, ( Uint8* ) &b );

		newData[( i *channels ) + 0] = r;
		newData[( i *channels ) + 1] = g;
		newData[( i *channels ) + 2] = b;

		pixel_value = 0;
	}
	gluBuild2DMipmaps( GL_TEXTURE_2D, 3, pBitmap[0]->w, pBitmap[0]->h, GL_RGB, GL_UNSIGNED_BYTE, newData );

	// Lastly, we need to tell OpenGL the quality of our texture map.  GL_LINEAR_MIPMAP_LINEAR
	// is the smoothest.  GL_LINEAR_MIPMAP_NEAREST is faster than GL_LINEAR_MIPMAP_LINEAR,
	// but looks blochy and pixilated.  Good for slower computers though.  Read more about
	// the MIN and MAG filters at the bottom of main.cpp
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );

	delete []newData;

	SDL_FreeSurface( pBitmap[0] );
}

//==============================================================================

#ifndef _DRAWINGS_H
#define _DRAWINGS_H

extern GLint pedestal, square;

////////////////////////////////////////////////////////////////////////////////
/// Klasa reprezentujaca krzywa Baziera.
////////////////////////////////////////////////////////////////////////////////
class Bazier {
	private:
	    /// Liczba punktow na krzywej.
		int ptsCounter;
		/// Wskaznik do tablicy z punktami krzywej.
		cVector3f *controlPts;
	public:	
		Bazier();
		Bazier( int XptsCounter, cVector3f *XcontrolPts ) {
				ptsCounter = XptsCounter;
				controlPts = XcontrolPts;
		}
	
	    /// Rysowanie krzywej Baziera.
		void draw( float width, int maxSteps );
		cVector3f pointOnCurve( cVector3f *point, int ptsCounter, float t );
};
////////////////////////////////////////////////////////////////////////////////
	void renderScene( void );
	void renderScene_HT( void );
	void initAllDLs( void );
	void initAllTextures( void );
////////////////////////////////////////////////////////////////////////////////
/// Skturkura bedaca odpowiednikiem pionka.
////////////////////////////////////////////////////////////////////////////////
	class cPawn {
		public:
			cPawn() {
				myLog << "Created: anonymous pawn.\n";
			};
			cPawn( int XbORw );
			~cPawn();
			//----------------------------------------------------------------------
			/// Kolor pionka.
			int bORw;
			float rotY;
			/// Czy jestem krolowa?
			bool queen;
			GLint pawnDL, selectedPawnDL;
			//----------------------------------------------------------------------
			/// Funkcja rysujaca pionek.
			void draw( bool selected, int XxBoard );
	};
////////////////////////////////////////////////////////////////////////////////
/// Klasa pola szachownicy.
////////////////////////////////////////////////////////////////////////////////
	class cField {
		public:
			cField();
			cField( int );
			cField( int, int );
			~cField();
			//----------------------------------------------------------------------
			int xBoard, attackedFrom;
			cPawn *occupiedBy;
			bool selected, occupied, underAttack, zbity, zaznaczony;
			//----------------------------------------------------------------------
			/// Rysujowanie pionka na tym polu.
			void drawPawn( void );
			/// Osadzanie pionka na tym polu.
			void occupy( cPawn *by );
			/// Usuwanie pionka z tego pola
			cPawn *deoccupy( void );
			/// Zaznaczanie tego pole.
			void select( void );
			/// Sprawdzanie, czy to pole jest w zasiegu ataku pionka na innym polu.
			void threaten( void );
			/// Bicie pionka.
			void zbij( void );
			void zaznacz( void );
			void odznacz( void );
			void relieve( void );
			void deselect( void );
			void postaw( void );
			void crownPawn( void );
			void displayDirections( void );
	};
//==============================================================================
	class pawnType {
		public:
			void( *pawnTypeWhite )( void );
			void( *pawnTypeBlack )( void );
			void( *pawnTypeSelected )( void );
	};
////////////////////////////////////////////////////////////////////////////////
/// Klasa szachownicy.
////////////////////////////////////////////////////////////////////////////////
	class cChessboard {
		public:
			cChessboard() {
				myLog << "Created: anonymous chessboard.\n";
			} cChessboard( float Xx, float Xy, float Xz );
			~cChessboard();
			//----------------------------------------------------------------------
			pawnType myPawnType;
			int selectedField;
			cField *myField[36];
			float x, y, z;
			bool whiteToMove;
			//----------------------------------------------------------------------
			/// Rysowanie planszy.
            void drawBoard( void );
            /// Plansza w wersji HT.
			void drawBoardHT( void );
			/// Rysowanie pionkow.
			void drawPawns( void );
			/// Rysuje plansze z pionkami.
			void show( void );
			/// Inicjalizuje polozenie poczatkowe pionkow.
			void initBoard( void );
			/// Aktualizuje dane.
			void update(int clickedField, int hoveredField);
			/// Przesuwa pionek z jednego pola na inne.
            void movePawn(int from, int destination);
            /// Glowna metoda sprawdzajaca czy dany ruch jest mozliwy.
            bool ifMovePossible(int from, int destination);
            /// Restartuje gre.
            void reInitBoard(void);
            /// Zwraca rodzaj pionka lezacego na danym polu.
            int coJestNaPolu(int pole);
            /// Sprawdza czy pionek jest przeznaczony do zbicia.
            bool jestZagrozony(int ktory=-1,bool tylko=false);
            /// Ustawia odpowiednie atrybuty polom.
            void odznaczWszystkie(void);
            /// Ustawia odpowiednie atrybuty polom.
            void postawWszystkie(bool pos);
            /// Ustawia odpowiednie atrybuty polom.
            void postawWszystkie2(void);
            /// Zwraca ilosc pionkow zagrozonych zbiciem.
            int ileZagrozonych(void);
            /// Zwraca ilosc bic dla danego pionka.
            int ileBic(int ktory);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int bezLG(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int bezLD(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int bezPG(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int bezPD(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int damkaLG(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int damkaLD(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int damkaPG(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int damkaPD(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int damkaSamoLG(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int damkaSamoLD(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int damkaSamoPG(int ktory, int kolor);
            /// Rozwija drzewo szukajace maksymalnej liczby bic.
            int damkaSamoPD(int ktory, int kolor);
            /// Sprawdza czy dana droga bicia pionkow dla damki jest mozliwa.
            int sprawdzLG(int ktory, int ile);
            /// Sprawdza czy dana droga bicia pionkow dla damki jest mozliwa.
            int sprawdzLD(int ktory, int ile);
            /// Sprawdza czy dana droga bicia pionkow dla damki jest mozliwa.
            int sprawdzPG(int ktory, int ile);
            /// Sprawdza czy dana droga bicia pionkow dla damki jest mozliwa.
            int sprawdzPD(int ktory, int ile);
            /// Sprawdza czy dana droga bicia pionkow dla damki jest mozliwa.
            int sprawdzSamoLG(int ktory, int ile);
            /// Sprawdza czy dana droga bicia pionkow dla damki jest mozliwa.
            int sprawdzSamoLD(int ktory, int ile);
            /// Sprawdza czy dana droga bicia pionkow dla damki jest mozliwa.
            int sprawdzSamoPG(int ktory, int ile);
            /// Sprawdza czy dana droga bicia pionkow dla damki jest mozliwa.
            int sprawdzSamoPD(int ktory, int ile);
            /// Metoda pomocnicz dla sprawdzenia poprawnosci ruchu.
            ///
            /// 0-PG 1-LG 2-LD 3-PD     
            bool znajdzD(int skad, int D, int kierunek);   
            /// Zwraca maksymalna liczbe bic na szachownicy.
            int maxBic(int kolor);
            /// Metoda pomocnicz dla sprawdzenia poprawnosci ruchu.
            int czteryCzyPiec(int ile,int F,int D);
	};
	extern cChessboard myChessboard;
	//==============================================================================



#endif // _DRAWINGS_H

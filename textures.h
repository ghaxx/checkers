/* This source has been formatted by an unregistered SourceFormatX */
/* If you want to remove this info, please register this shareware */
/* Please visit http://www.textrush.com to get more information    */

#ifndef _TEXTURES_H
	#define _TEXTURES_H

	#define MAX_TEXTURES 4

	extern unsigned int textureList[MAX_TEXTURES];
	//==============================================================================
	void createTexture( unsigned int textureArray[], char *strFileName, int textureID );

#endif // _TEXTURES_H

#include "init.h"
#include "drawings.h"
#include "textures.h"
#include "peripherals.h"


cMouse myMouse;
cKeyboard myKeyboard;
////////////////////////////////////////////////////////////////////////////////
cKeyboard::cKeyboard() {
	fPressed = false;
	upPressed = false;
	downPressed = false;
	leftPressed = false;
	rightPressed = false;
	wPressed = false;
	sPressed = false;
	aPressed = false;
	dPressed = false;
	qPressed = false;
	ePressed = false;
	spacePressed = false;
}
////////////////////////////////////////////////////////////////////////////////
cMouse::cMouse() {
	mouseDown = false;
	leftDown = false;
	rightDown = false;
	middleDown = false;

	hitDown =  - 1;
	hitUp =  - 1;
	selectedObject =  - 1;
	clickedObject =  - 1;
}
////////////////////////////////////////////////////////////////////////////////
void cKeyboard::handleKeyPressEvent( SDL_keysym *keysym ) {
	switch ( keysym->sym ) {
		case SDLK_l:
			bLight = !bLight;
			bLight ? glEnable( GL_LIGHTING ): glDisable( GL_LIGHTING );
			break;
		case SDLK_F1:
			toggleFullScreen();
			break;
			// moving flat
		case SDLK_UP:
			upPressed = true;
			break;
		case SDLK_DOWN:
			downPressed = true;
			break;
		case SDLK_LEFT:
			leftPressed = true;
			break;
		case SDLK_RIGHT:
			rightPressed = true;
			break;
			// moving 3D
		case SDLK_w:
			wPressed = true;
			break;
		case SDLK_s:
			sPressed = true;
			break;
		case SDLK_a:
			aPressed = true;
			break;
		case SDLK_d:
			dPressed = true;
			break;
			// vertical moves
		case SDLK_q:
			qPressed = true;
			break;
		case SDLK_e:
			ePressed = true;
			break;
		case SDLK_SPACE:
		    spacePressed = true;
		    break;
		case SDLK_ESCAPE:
			quit( 0 );
		default:
			break;
	}
}
////////////////////////////////////////////////////////////////////////////////
void cKeyboard::handleKeyReleaseEvent( SDL_keysym *keysym ) {
	switch ( keysym->sym ) {
		// moving flat
		case SDLK_UP:
			upPressed = false;
			break;
		case SDLK_DOWN:
			downPressed = false;
			break;
		case SDLK_LEFT:
			leftPressed = false;
			break;
		case SDLK_RIGHT:
			rightPressed = false;
			break;
			// moving 3D
		case SDLK_w:
			wPressed = false;
			break;
		case SDLK_s:
			sPressed = false;
			break;
		case SDLK_a:
			aPressed = false;
			break;
		case SDLK_d:
			dPressed = false;
			break;
			// vertical moves
		case SDLK_q:
			qPressed = false;
			break;
		case SDLK_e:
			ePressed = false;
			break;
		case SDLK_SPACE:
		    spacePressed = false;
		    break;
		default:
			break;
	}
}
////////////////////////////////////////////////////////////////////////////////
void cMouse::handleMouseDownEvent( SDL_MouseButtonEvent *button ) {
	switch ( button->button ) {
		case SDL_BUTTON_LEFT:
			leftDown = true;
			hitDown = theObject;
			break;
		case SDL_BUTTON_RIGHT:
			rightDown = true;
			break;
		case SDL_BUTTON_MIDDLE:
			middleDown = true;
			break;
		default:
			break;
	}
}
////////////////////////////////////////////////////////////////////////////////
void cMouse::handleMouseUpEvent( SDL_MouseButtonEvent *button ) {
	switch ( button->button ) {
		case SDL_BUTTON_LEFT:
			leftDown = false;
			hitUp = theObject;
			if ( hitUp == hitDown ) {
				selectedObject = hitUp;
				clickedObject = hitUp;
				hitUp =  - 1;
				hitDown =  - 2;
			}
			 else {
				if ( selectedObject > 0 ) {
					hitUp =  - 1;
					hitDown =  - 2;
					selectedObject =  - 1;
				}
			}
			break;
		case SDL_BUTTON_RIGHT:
			rightDown = false;
			break;
		case SDL_BUTTON_MIDDLE:
			middleDown = false;
			break;
		default:
			break;
	}
}
////////////////////////////////////////////////////////////////////////////////

/// Wraz z nowa scena przepada informacja o kliknietym obiekcie w poprzedniej.
/// Jednak zmienna selectedObject wciaz ja przechowuje.
void cMouse::update() {
	clickedObject =  - 1;
	SDL_GetMouseState( &x, &y );
}
////////////////////////////////////////////////////////////////////////////////
int cMouse::getX() {
	return x;
}
////////////////////////////////////////////////////////////////////////////////
int cMouse::getY() {
	return y;
}
////////////////////////////////////////////////////////////////////////////////
int cMouse::getClicked() {
    return clickedObject;
}
////////////////////////////////////////////////////////////////////////////////
int cMouse::getSelected() {
    return selectedObject;
}
////////////////////////////////////////////////////////////////////////////////


#ifndef _CAMERA_H
	#define _CAMERA_H

	extern float kSpeed;

	class cCamera {
		public:
			cVector3f m_vPosition;
			cVector3f m_vView;
			cVector3f m_vUpVector;
			cVector3f m_vStrafe;

			cVector3f position() {
					return m_vPosition;
			} cVector3f view() {
					return m_vView;
			}
			cVector3f upVector() {
					return m_vUpVector;
			}
			cVector3f strafe() {
					return m_vStrafe;
			}

			cCamera();
			void positionCamera( float positionX, float positionY, float positionZ, float viewX, float viewY, float viewZ, float upVectorX, float upVectorY, float upVectorZ );
			void moveCamera( float speed );
			void moveCameraFlat( float speed );
			void strafeCamera( float speed );
			void verticalMove( float speed );
			void setViewByMouse( int mouseX, int mouseY );
			void setViewByMouseNWarp( int mouseX, int mouseY );
			void rotateView( float angle, float x, float y, float z );
			void rotateAroundPoint( cVector3f vCenter, float angle, float x, float y, float z );
			void rotateAroundViewByMouse( int mouseX, int MouseY );
			void rotateAroundChessboard( int mouseX, int mouseY );
			void checkForMovement( void );
			void update( void );
			void look( void );
	};
	extern cCamera myCamera;


#endif // _CAMERA_H

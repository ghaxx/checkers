#include "init.h"
#include "camera.h"
#include "drawings.h"
#include "textures.h"
#include "peripherals.h"
#include "menu.h"


/// Glowna petla programu. 
void mainLoop( void ) {

	SDL_Event event;

	if ( !noTexturing ) {
		// Wczytuje tekstury, o ile nie uruchomiono z parametrem "noTexturing".
		initAllTextures();
	}

	initAllDLs(); // Inicjalizacja "list rysowania" (? draw lists).

	while ( !bDone ) {
		std::cout << "dupa\n";
		std::cout << "---\n";
		// Obsluga interakcji z aplikacja.
		while ( SDL_PollEvent( &event )) {
			switch ( event.type ) {
			    // Koniec pracy.
				case SDL_QUIT:
					bDone = true;
					break;

				// Wcisniecie przycisku myszy.
				case SDL_MOUSEBUTTONDOWN:
					myMouse.handleMouseDownEvent( &event.button );
					break;

				// Puszczenie przycisku myszy.
				case SDL_MOUSEBUTTONUP:
					myMouse.handleMouseUpEvent( &event.button );
					break;

				// Wcisniecie lawisza na klawiaturze.
				case SDL_KEYDOWN:
					myKeyboard.handleKeyPressEvent( &event.key.keysym );
					break;

				// Puszczenie klawisza klawiatury.
				case SDL_KEYUP:
					myKeyboard.handleKeyReleaseEvent( &event.key.keysym );
					break;
					
                // Zmiana rozmiaru okna.
                case SDL_VIDEORESIZE:
                    mainWindow = SDL_SetVideoMode(event.resize.w, event.resize.h, SCREEN_DEPTH, videoFlags);
                    sizeOpenGLScreen(event.resize.w, event.resize.h);
                    if(mainWindow == NULL){
                    myLog << "Failed resizing SDL window: " << SDL_GetError() << endl;
                        quit(0);
                    }
                break;

				default:
					break;
			} // switch
		} // while( SDL_ ...


		// Aktualizacje pozycji kamery, stanu shachownicy i myszy.
		myCamera.update();
		myChessboard.update( myMouse.getClicked(), theObject );
		myMouse.update();

		// Sprawdzanie, na co wskazuje mysz.
		
		// Jesli mysz nie jesy nad menu...
		if ( !myGUI.update()) {
			startPicking( myMouse.getX(), myMouse.getY());
			renderScene_HT();
			stopPicking();
		// ... jesli jest nad menu, to nie wskazuje na obiekty sceny.
		} else {
			theObject =  - 1;
		}

		// Rysowanie sceny.
		renderScene();

	} // while(!bDone)

}

/// Sprawdzanie, jakimi parametrami uruchomiono aplikacje. 

/// Atrybuty:
///  - altGLuint: na niektorych kartach grafiki umozliwa zaznaczanie pionkow mysza. Jakby zaznaczanie nie dzialalo, gre trzeba uruchomic z tym parametrem.
///  - noTexturing: wylaczenie tekstur.
void parseArgs( int argc, char *argv[] ) {
	string c;
	myLog << "Liczba parametrow: " << argc << "\n";
	for ( int i = 0; i < argc; i++ ) {
		c = argv[i];
		if ( c == "-altGLuint" ) {
			altGLuint = true;
		}
		if ( c == "-noTexturing" ) {
			noTexturing = true;
		}
		myLog << "| Parametr nr " << i << ": " << c << "\n";
	}
	myLog << "To juz wszystkie parametry...\n";
}

/// Funkcja main() aplikacji. Oto ona... 
int main( int argc, char *argv[] ) {
    // Jesli nie uda sie utworzyc okienka...
	if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
		// ...wpisujemy do logu odpowiednia linijke...
		myLog << "Blad inicjalizacji SDL Video : " << SDL_GetError() << "\n"; 
		 // ... i konczymy zabawe.
        return 1;
	}
	
	start();

	// Zaczyna sie wlasciwy kod programu.
	myLog << "--- Starting mainLoop() ---\n";
	mainLoop();
	myLog << "--- Completed mainLoop() ---";
	myLog.close();
	return 0;
}

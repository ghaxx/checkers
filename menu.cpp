#include "init.h"
#include "camera.h"
#include "drawings.h"
#include "textures.h"
#include "peripherals.h"
#include "menu.h"


cGUI myGUI;
bool onGUI;

////////////////////////////////////////////////////////////////////////////////
void finishIt() {
	bDone = true;
}
////////////////////////////////////////////////////////////////////////////////
void restart() {
	myChessboard.reInitBoard();
}
////////////////////////////////////////////////////////////////////////////////
/// Pusta funkcja ja domyslna dla elementow menu
////////////////////////////////////////////////////////////////////////////////

void emptyFunc(){}

////////////////////////////////////////////////////////////////////////////////
/// Tworzy element menu
////////////////////////////////////////////////////////////////////////////////

cMenuItem::cMenuItem( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue ) {
	x = Xx;
	y = Xy;
	width = Xwidth;
	Xheight < 20 ? height = 20: height = Xheight;
	value = Xvalue;
	highlighted = false;
	down = false;
	clicked = false;
	BGColor = cColor( 0, 0, 0 );
	FGColor = cColor( 1, 1, 1 );
	HLColor = cColor( 0.2, 0.2, 0.5 );
	valueLenght = 0;
	while ( value[valueLenght++] )
		;
	if ( valueLenght *9 > width ) {
		width = 10+( valueLenght *9 );
	}
	onClick = 0;
	menu = 0;
	onMouseOver = 0;
};

////////////////////////////////////////////////////////////////////////////////
/// Tworzy element menu
////////////////////////////////////////////////////////////////////////////////

cMenuItem::cMenuItem( int Xx, int Xy, char *Xvalue ) {
	x = Xx;
	y = Xy;
	height = 20;
	value = Xvalue;
	highlighted = false;
	down = false;
	clicked = false;
	BGColor = cColor( 0, 0, 0 );
	FGColor = cColor( 1, 1, 1 );
	HLColor = cColor( 0.2, 0.2, 0.5 );
	valueLenght = 0;
	while ( value[valueLenght++] )
		;
	width = 5+( valueLenght *9 );
	onClick = 0;
	menu = 0;
	onMouseOver = 0;
};

////////////////////////////////////////////////////////////////////////////////
/// Tworzy element menu
////////////////////////////////////////////////////////////////////////////////

cMenuItem::cMenuItem( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue, cMenu *Xmenu ) {
	x = Xx;
	y = Xy;
	width = Xwidth;
	Xheight < 20 ? height = 20: height = Xheight;
	value = Xvalue;
	highlighted = false;
	down = false;
	clicked = false;
	BGColor = cColor( 0, 0, 0 );
	FGColor = cColor( 1, 1, 1 );
	HLColor = cColor( 0.2, 0.2, 0.5 );
	valueLenght = 0;
	while ( value[valueLenght++] )
		;
	if ( valueLenght *9 > width ) {
		width = 10+( valueLenght *9 );
	}
	onClick = 0;
	onMouseOver = 0;
	menu = Xmenu;
};

////////////////////////////////////////////////////////////////////////////////
/// Tworzy element menu
////////////////////////////////////////////////////////////////////////////////

cMenuItem::cMenuItem( int Xx, int Xy, char *Xvalue, void( *XonClick )(), void( *XonMouseOver )()) {
	x = Xx;
	y = Xy;
	height = 20;
	value = Xvalue;
	highlighted = false;
	down = false;
	clicked = false;
	BGColor = cColor( 0, 0, 0 );
	FGColor = cColor( 1, 1, 1 );
	HLColor = cColor( 0.2, 0.2, 0.5 );
	valueLenght = 0;
	while ( value[valueLenght++] )
		;
	width = 5+( valueLenght *9 );
	menu = 0;
	onClick = XonClick;
	onMouseOver = XonMouseOver;
};

////////////////////////////////////////////////////////////////////////////////
/// Tworzy element menu
////////////////////////////////////////////////////////////////////////////////

cMenuItem::cMenuItem( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue, void( *XonClick )()) {
	x = Xx;
	y = Xy;
	width = Xwidth;
	Xheight < 20 ? height = 20: height = Xheight;
	value = Xvalue;
	highlighted = false;
	down = false;
	clicked = false;
	BGColor = cColor( 0, 0, 0 );
	FGColor = cColor( 1, 1, 1 );
	HLColor = cColor( 0.2, 0.2, 0.5 );
	valueLenght = 0;
	while ( value[valueLenght++] )
		;
	if ( valueLenght *9 > width ) {
		width = 10+( valueLenght *9 );
	}
	onClick = XonClick;
	onMouseOver = 0;
	menu = 0;
};

////////////////////////////////////////////////////////////////////////////////
/// Tworzy menu
////////////////////////////////////////////////////////////////////////////////

cMenu::cMenu( int XitemsNo, int Xx, int Xy, char *Xitem[], void( *itemFunc[] )()) {
	menuItem = new cMenuItem[XitemsNo];
	for ( int i = 0; i < XitemsNo; i++ ) {
		menuItem[i] = cMenuItem( Xx, Xy + ( i *20 ), 100, 20, & *Xitem[i], itemFunc[i] );
	}
	itemsNo = XitemsNo;
	visible = false;
	x = Xx;
	y = Xy;
	width = 100;
	height = itemsNo * 20;
}

////////////////////////////////////////////////////////////////////////////////
/// Tworzy okienko potwierdzenia (tak/nie)
////////////////////////////////////////////////////////////////////////////////

cPrompt::cPrompt( int Xx, int Xy, int Xwidth, int Xheight, char *Xquestion ) {
	x = Xx;
	y = Xy;
	width = Xwidth;
	height = Xheight;
	which = 0;
	question = Xquestion;
	OK = new cButton( x, y + height - 35, width / 2, 30, "OK" );
	cancel = new cButton( x + ( width / 2 ), y + height - 35, width / 2, 30, "Cancel" );
}

////////////////////////////////////////////////////////////////////////////////
/// Rysuje okienko potwierdzenia (tak/nie)
////////////////////////////////////////////////////////////////////////////////

int cPrompt::draw() {
	//onGUI = true;
	int which = 0;
	glBegin( GL_QUADS );
	glVertex2i( x, y + height );
	glVertex2i( x + width, y + height );
	glVertex2i( x + width, y );
	glVertex2i( x, y );
	glEnd();
	write( x - 10, y - 10, question );
	OK->update();
	cancel->update();
	if ( OK->clicked ) {
		which = 1;
	}
	if ( cancel->clicked ) {
		which = 2;
	}
	OK->draw();
	cancel->draw();
	return which;
}

////////////////////////////////////////////////////////////////////////////////
/// Rysuje element menu
////////////////////////////////////////////////////////////////////////////////

void cMenuItem::draw() {
	bool inOrtho = bOrtho;
	if ( !inOrtho ) {
		orthoBegin();
	}
	highlighted ? glColor3f( HLColor.r, HLColor.g, HLColor.b ): glColor3f( BGColor.r, BGColor.g, BGColor.b );
	glBegin( GL_QUADS );
	glVertex2i( x, y + height );
	glVertex2i( x + width, y + height );
	glVertex2i( x + width, y );
	glVertex2i( x, y );
	glEnd();
	glColor3f( FGColor.r, FGColor.g, FGColor.b );
	write( x + 5+( width - ( valueLenght *9 )) / 2, y + ( height / 2 ) + 5, value );
	if ( !inOrtho ) {
		orthoEnd();
	}
}

////////////////////////////////////////////////////////////////////////////////
/// Rysuje przycisk
////////////////////////////////////////////////////////////////////////////////

void cButton::draw() {

	bool inOrtho = bOrtho;
	if ( !inOrtho ) {
		orthoBegin();
	}
	//myLog << "Drawing: " << x << "\n";
	highlighted ? glColor3f( HLColor.r, HLColor.g, HLColor.b ): glColor3f( BGColor.r, BGColor.g, BGColor.b );
	glBegin( GL_QUADS );
	glVertex2i( x, y + height );
	glVertex2i( x + width, y + height );
	glVertex2i( x + width, y );
	glVertex2i( x, y );
	glEnd();
	glColor3f( FGColor.r, FGColor.g, FGColor.b );
	glBegin( GL_LINE_LOOP );
	glVertex2i( x, y + height );
	glVertex2i( x + width, y + height );
	glVertex2i( x + width, y );
	glVertex2i( x, y );
	glEnd();
	glColor3f( FGColor.r, FGColor.g, FGColor.b );
	write( x + 5+( width - ( valueLenght *9 )) / 2, y + ( height / 2 ) + 5, value );
	if ( !inOrtho ) {
		orthoEnd();
	}
}

////////////////////////////////////////////////////////////////////////////////
/// Rysuje menu
////////////////////////////////////////////////////////////////////////////////

void cMenu::draw() {
	if ( visible ) {
		glColor3f( 1, 1, 1 );
		glBegin( GL_QUADS );
		glVertex2i( x - 1, y + height + 1 );
		glVertex2i( x + width + 1, y + height + 1 );
		glVertex2i( x + width + 1, y - 1 );
		glVertex2i( x - 1, y - 1 );
		glEnd();
		for ( int i = 0; i < itemsNo; i++ ) {
			menuItem[i].draw();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
/// Odswieza menu
////////////////////////////////////////////////////////////////////////////////

bool cMenu::update() {
	onMenu = false;
	bool notOnClick = true;
	if ( visible ) {
		for ( int i = 0; i < itemsNo; i++ ) {
			if ( menuItem[i].update()) {
				onMenu = true;
			}
			if ( menuItem[i].clicked ) {
				notOnClick = false;
			}
		}
	}
	return onMenu &notOnClick;
}

////////////////////////////////////////////////////////////////////////////////
/// Odswieza element menu
////////////////////////////////////////////////////////////////////////////////

bool cMenuItem::update() {
	if ( myMouse.getX() >= x && myMouse.getX() <= x + width && myMouse.getY() >= y && myMouse.getY() <= y + height ) {
		highlighted = true;
		onGUI = true;
		if ( myMouse.leftDown ) {
			down = true;
		}
		if ( !down ) {
			clicked = false;
		}
		if ( !myMouse.leftDown && down ) {
			clicked = true;
			down = false;
		}
	} else {
		highlighted = false;
		clicked = false;
	}
	if ( highlighted && onMouseOver ) {
		onMouseOver();
	}
	if ( menu ) {
		if ( highlighted ) {
			menu->visible = true;
			if ( menu->update()) {
				highlighted = false;
			}
		} else {
			if ( menu->update()) {
				menu->visible = true;
			} else {
				menu->visible = false;
			};
		}
	}
	if ( clicked && onClick ) {
		onClick();
	}
	return highlighted;
}

////////////////////////////////////////////////////////////////////////////////
/// Rysuje cale GUI
////////////////////////////////////////////////////////////////////////////////

void cGUI::draw() {
	// static int x = 190;
	// glTranslatef(-x,0,0);
	//    if(myMouse.x <= x) x = 0;
	//   if(myMouse.x > x) x = 190;
	glBlendFunc( GL_SRC_ALPHA, GL_SRC_COLOR );
	glEnable( GL_BLEND );

	glColor4f( 0.1, 0.1, 0.1, 0.5 );
	glBegin( GL_QUADS );
	glVertex2i( 0, 0 );
	glVertex2i( 0, SCREEN_HEIGHT );
	glVertex2i( 200, SCREEN_HEIGHT );
	glVertex2i( 200, 0 );
	glEnd();
	glDisable( GL_BLEND );
	glColor3f( 1, 1, 1 );
	exitButton->draw();
	boardReInitButton->draw();
	gameButton->draw();
	gameMenu->draw();
}

////////////////////////////////////////////////////////////////////////////////
/// Inicjalizacja GUI
////////////////////////////////////////////////////////////////////////////////

void cGUI::init() {
	char *items[2] =  {
		"Restart", "Quit"
	};
	void(( *itemFunc[2] )( void )) =  {
		restart, finishIt
	};
	gameMenu = new cMenu( 2, 20, 70, items, itemFunc );
	gameButton = new cButton( 10, 50, 180, 30, "Game", gameMenu );
	exitButton = new cButton( 10, SCREEN_HEIGHT - 40, 180, 30, "Exit", finishIt );
	boardReInitButton = new cButton( 10, SCREEN_HEIGHT - 80, 180, 30, "Restart", emptyFunc );
	cPrompt *finish = new cPrompt( 200, 200, 300, 200, "koniec?" );

}

////////////////////////////////////////////////////////////////////////////////
/// Odswiezanie GUI
////////////////////////////////////////////////////////////////////////////////

bool cGUI::update() {
	onGUI = false;
	exitButton->update();
	gameButton->update();
	boardReInitButton->update();

	//gameMenu->update();
	return onGUI;
}
////////////////////////////////////////////////////////////////////////////////

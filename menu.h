/* This source has been formatted by an unregistered SourceFormatX */
/* If you want to remove this info, please register this shareware */
/* Please visit http://www.textrush.com to get more information    */

#ifndef _MENU_H
#define _MENU_H

////////////////////////////////////////////////////////////////////////////////
/// Deklaracja klasy.
////////////////////////////////////////////////////////////////////////////////
class cMenu;

////////////////////////////////////////////////////////////////////////////////
/// Klasa obiektu na liscie w rozwijalnym menu.
////////////////////////////////////////////////////////////////////////////////
class cMenuItem {
	public:
		cMenuItem(){};
		cMenuItem( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue );
		cMenuItem( int Xx, int Xy, char *Xvalue );
		cMenuItem( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue, cMenu *Xmenu );
		cMenuItem( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue, void( *XonClick )());
		cMenuItem( int Xx, int Xy, char *Xvalue, void( *XonClick )(), void( *XonMouseOver )());
		//----------------------------------------------------------------------
		/// Dlugosc napisu.
        int valueLenght;
        /// Pozycja x.
		int x;
		/// Pozycja y.
		int y;
		/// Szerokosc obiektu.
		int width;
		/// Wysokosc obiektu.
		int height;
		/// Kolor napisu.
		cColor FGColor;
		/// Kolor tla.
		cColor BGColor;
		/// Kolor podswietlenia.
		cColor HLColor;
		bool clicked, down;
		char *value;
		bool highlighted;
		cMenu *menu;
		//----------------------------------------------------------------------
		/// Rysowanie obiektu.
		void draw( void );
		/// Sprawdzanie, czy obiekt jest zaznaczony, czy tylko podswietlany, czy w ogole poza kregiem zainteresowania.
		bool update( void );
		/// Funkcja wykonywana, gdy przysick zostanie klikniety.
		void( *onClick )( void );
		/// Funkcja wywolywala, gdy kursor myszy znajduje sie na przyciskiem.
		void( *onMouseOver )( void );
};
////////////////////////////////////////////////////////////////////////////////
/// Klasa rozwijalnego menu.
////////////////////////////////////////////////////////////////////////////////

/// Takie menu sklada sie z obiektow menuItem.
class cMenu {
	public:
		cMenu(){}
		cMenu( int XitemsNo, int Xx, int Xy, char *Xitem[], void( *itemFunc[] )());
		//----------------------------------------------------------------------
		int x, y, width, height;
		cMenuItem *menuItem;
		/// Ilosc opcji w menu.
        int itemsNo;
        /// Czy menu jest wyswietlane?
		bool visible, 
        /// Czy mysz jest aktualnie nad menu    .
             onMenu;
		//----------------------------------------------------------------------
		void draw( void );
		bool update( void );
		//----------------------------------------------------------------------
	};
////////////////////////////////////////////////////////////////////////////////
/// Prosty przycisk.
////////////////////////////////////////////////////////////////////////////////

/// Jest to obiekt klasy menuItem z dodana ramka i mozliwoscia "podpiecia"
/// rozwijalnego menu klasy cMenu.
class cButton: public cMenuItem {
	public:
		cButton() {
				cMenuItem();
		} cButton( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue ): cMenuItem( Xx, Xy, Xwidth, Xheight, Xvalue ) {
				menuAttached = false;
		}
		cButton( int Xx, int Xy, char *Xvalue ): cMenuItem( Xx, Xy, Xvalue ) {
				menuAttached = false;
		}
		cButton( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue, cMenu *Xmenu ): cMenuItem( Xx, Xy, Xwidth, Xheight, Xvalue, Xmenu ) {
				menuAttached = true;
				menu = Xmenu;
		}
		cButton( int Xx, int Xy, int Xwidth, int Xheight, char *Xvalue, void( *XonClick )( void )): cMenuItem( Xx, Xy, Xwidth, Xheight, Xvalue, XonClick ) {
				menuAttached = false;
		}
		//----------------------------------------------------------------------
		/// Czy podpieto menu?
		bool menuAttached;
		/// Wskaznik do podpietego menu.
		cMenu *menu;
		//----------------------------------------------------------------------
		/// Rysowanie obiektu.
		void draw( void );

};
////////////////////////////////////////////////////////////////////////////////
/// Pole tekstowe.
////////////////////////////////////////////////////////////////////////////////
class cTextArea {
	public:
	    char *text[];
	    int x, y, width, height;
	};
////////////////////////////////////////////////////////////////////////////////
/// Okienko potwierdzania decyzji.
////////////////////////////////////////////////////////////////////////////////
class cPrompt :public cTextArea, cButton {
    public:
		cPrompt(){}
		cPrompt( int Xx, int Xy, int Xwidth, int Xheight, char *Xquestion );
		//----------------------------------------------------------------------
		int x, y, width, height;
		char *question;
		cButton *OK;
		cButton *cancel;
		int which;
		int draw( void );
		//        void pressOK(void);
		//        void pressCancel(void);
};

////////////////////////////////////////////////////////////////////////////////
/// Klasa interfejsu urzytkownika - definicja.
////////////////////////////////////////////////////////////////////////////////
class cGUI {
	public:
		cGUI(){};
		cButton *exitButton;
		cButton *boardReInitButton;
		cButton *gameButton;
		cTextArea *hits;
		cPrompt *finish;
		cMenu *gameMenu;
		/// Rysowanie interfejsu (tryb 2D).
		void draw();
		/// Inicjalizacja menu.
		void init();
		/// Sprawdzanie, czym aktualnie jest zainteresowany uzytkownik.
		bool update();
};
extern cGUI myGUI;

#endif // _MENU_H

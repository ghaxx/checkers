#include "init.h"
#include "camera.h"
#include "drawings.h"
#include "textures.h"
#include "peripherals.h"
#include "menu.h"

GLint pedestal,  /// Draw list dla podstawy szachownicy.
      square;    /// Draw list dla pol shachownicy.
cChessboard myChessboard( 5, 5, 0 );

	int Pascal( int pscRow, int pscCol );
	void drawSphere( float x, float y, float z, double radius, int precision );
	void Earth( float x, float y, float z, double radius, int precision );
	void Earth_HT( float x, float y, float z, double radius, int precision );
	void markQueen( void );
	void drawPawns( void );
	void initPawns( void );
	void chessboard( float x, float y, float z, int *pi, int *pj, int theObject );
	void chessboard_HT( float x, float y, float z, int *pi, int *pj, int theObject );
	void drawPedestal( void );
	void drawSquare( void );
	void drawFuturisticPawnWhite( void );
	void drawFuturisticPawnBlack( void );
	void drawFuturisticPawnSelected( void );
	void drawCube( float x, float y, float z, float sizeX, float sizeY, float sizeZ );
	void axes( void );
	void renderOrthoAddons( void );
	void lights( void );
	GLint initDL( void( *myList )( void ));


////////////////////////////////////////////////////////////////////////////////
/// Rysowanie sceny.
////////////////////////////////////////////////////////////////////////////////
void renderScene() {
	float static xxx = 0;
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity();
	/// Ustawianie kamery.
	myCamera.look();
	if(bLight) lights();
	/// Rysowanie shachownicy.
	myChessboard.show();
	if( myKeyboard.spacePressed ) {
	    Earth( 0, 0, 0, 4, 20 );    
	}    
	glColor3f( 1, 0, 1 );
	/// Wyswietlanie dodatkowych elementow 2D.
	renderOrthoAddons();
	SDL_GL_SwapBuffers();
}
////////////////////////////////////////////////////////////////////////////////
/// Rysowanie sceny specjalnie na potrzeby wyszukiwania obiektow pod mysza.
////////////////////////////////////////////////////////////////////////////////

/// Nie rysuje wiekszosci obiektow, ktore nie sa brane pod uwage przy "detekcji
/// bycia pod mysza". Mozna zamiast niej uzyc standardowej funkcji rysujacej, ale
/// dzieki temu zyskuje sie troche na szybkosci kodu.
void renderScene_HT( void ) {
	float static xxx = 0;
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity();
	myCamera.look();
	myChessboard.drawBoardHT();
	SDL_GL_SwapBuffers();
}
////////////////////////////////////////////////////////////////////////////////
/// Ustawia swiatla na scenie.
////////////////////////////////////////////////////////////////////////////////

/// Steruje mruganiem swiatel. W przeciwienstwie do funkcji renderScene_HT(), ta
/// cala aplikacje. 
///
/// Ale poprawia jej wyglad...
void lights() {
	static float g = 0.4, gX = 0.03;
	if ( g <= 0 || g >= 0.4 ) {
		gX =  - gX;
	}
	g += gX;
	float g_LightPosition[4] =  {
		myCamera.m_vPosition.x, myCamera.m_vPosition.y, myCamera.m_vPosition.z, 1
	};
	float ambience[4] =  {
		0.8f, 0.8f, 0.8f, 1.0f
	};
	float diffuse[4] =  {
		1.0f, 1.0f, 1.0f, 1.0f
	};
	glLightfv( GL_LIGHT0, GL_AMBIENT, ambience );
	glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse );
	glLightfv( GL_LIGHT0, GL_POSITION, g_LightPosition );
	glEnable( GL_LIGHT0 );

	float g_LightPosition1[4] = { -2.0f, 13.0f, 3.0f, 1.0f };
	float ambience1[4] =        {  0.0f,  0.0f, 0.0f, 1.0f };
	float diffuse1[4] =         {     g,  0.0f, 0.0f, 1.0f };
	glLightfv( GL_LIGHT1, GL_AMBIENT, ambience1 );
	glLightfv( GL_LIGHT1, GL_DIFFUSE, diffuse1 );
	glLightfv( GL_LIGHT1, GL_POSITION, g_LightPosition1 );
	glEnable( GL_LIGHT1 );

	float g_LightPosition2[4] =  { 13.0f, -2.0f,     3.0f, 1.0f };
	float ambience2[4] =         {  0.0f,  0.0f,     0.0f, 1.0f };
	float diffuse2[4] =          {  0.0f,  0.0f, 0.4f + g, 1.0f };
	glLightfv( GL_LIGHT2, GL_AMBIENT, ambience2 );
	glLightfv( GL_LIGHT2, GL_DIFFUSE, diffuse2 );
	glLightfv( GL_LIGHT2, GL_POSITION, g_LightPosition2 );
	glEnable( GL_LIGHT2 );
}
////////////////////////////////////////////////////////////////////////////////
/// Wyswietla dwuwymiarowe dodatki. Glownie chodzi o menu...
////////////////////////////////////////////////////////////////////////////////
void renderOrthoAddons( void ) {
	glDisable( GL_LIGHTING );
	orthoBegin();

	myGUI.draw();

/*	glBindTexture( GL_TEXTURE_2D, textureList[2] );
	
	glBegin( GL_QUADS );

	glTexCoord2f( 0.0f, 0.0f );
	glVertex2i( 10, 100 ); // Bottom Left (0,0)

	glTexCoord2f( 1.0f, 0.0f );
	glVertex2i( 100, 100 ); // Bottom Right

	glTexCoord2f( 1.0f, 1.0f );
	glVertex2i( 100, 10 ); // Top Right

	glTexCoord2f( 0.0f, 1.0f );
	glVertex2i( 10, 10 ); // Top Left (0,1)
	glEnd();
*/
	orthoEnd();
	glEnable( GL_LIGHTING );
}
////////////////////////////////////////////////////////////////////////////////
/// Laduje wszystkie uzywane tekstury do pamieci.
////////////////////////////////////////////////////////////////////////////////
void initAllTextures() {
	createTexture( textureList, "textures/bitmap.bmp", 0 );
	createTexture( textureList, "textures/square.bmp", 1 );
	createTexture( textureList, "textures/earth.bmp", 2 );
	createTexture( textureList, "textures/drzewo.bmp", 3 );
}
////////////////////////////////////////////////////////////////////////////////
/// Inicjalizuje "listy rysowania".
////////////////////////////////////////////////////////////////////////////////
void initAllDLs() {
	pedestal = initDL( drawPedestal );
	square = initDL( drawSquare );
}
////////////////////////////////////////////////////////////////////////////////
/// Funkcja tworzaca "liste rysowania" z szeregu komend rysujacych "obiekty prymitywne".
////////////////////////////////////////////////////////////////////////////////

/// ...Tutaj wyraznie odczuwam problemy z tlumaczeniem tego, czego nauczylem sie
/// z angielskich tutoriali, na jezyk polski... This function converts set of 
/// primitives into a draw list.
GLint initDL( void( *myList )( void )) {
	GLint tmpList;
	tmpList = glGenLists( 1 );
	glNewList( tmpList, GL_COMPILE );
	myList();
	glEndList();
	return tmpList;
}
////////////////////////////////////////////////////////////////////////////////
/// Procedura rysujaca Ziemie...
////////////////////////////////////////////////////////////////////////////////
void Earth( float x, float y, float z, double radius, int precision ) {
	static float earthRotation = 0;
	GLUquadricObj *pObj = gluNewQuadric();
	gluQuadricTexture( pObj, true );
	if ( !noTexturing ) {
		glBindTexture( GL_TEXTURE_2D, textureList[2] );
	}
	glColor3f( 0.8, 0.9, 1 );
	if ( theObject == 100 ) {
		glColor3f( 1, 0.6, 0.6 );
	}
	glPushMatrix();
	glTranslatef( x, y, z );
	glRotatef(  - 116, 1, 0, 0 );
	glBegin( GL_LINES );
	glVertex3f( 0, 0,  - 100 );
	glVertex3f( 0, 0, 100 );
	glEnd();
	glPushMatrix();
	glRotatef( earthRotation += 0.3, 0, 0, 1 );
	gluSphere( pObj, radius, precision, precision );
	glPopMatrix();
	glPopMatrix();
	gluDeleteQuadric( pObj );
}
////////////////////////////////////////////////////////////////////////////////
/// Ziemia w trybie HitTest.
////////////////////////////////////////////////////////////////////////////////
void Earth_HT( float x, float y, float z, double radius, int precision ) {
	GLUquadricObj *pObj = gluNewQuadric();
	gluQuadricTexture( pObj, true );
	glPushMatrix();
	glTranslatef( x, y, z );
	glPushName( 100 );
	gluSphere( pObj, radius, precision, precision );
	glPopName();
	glPopMatrix();
	gluDeleteQuadric( pObj );
}
////////////////////////////////////////////////////////////////////////////////
/// Rysowanie "drucika" w pionku.
////////////////////////////////////////////////////////////////////////////////

/// Zwykla krzywa baziera.
void Bazier::draw( float width, int maxSteps ) {
	cVector3f vPoint;
	float maxStepsM1 = 1.0f / maxSteps;
	glLineWidth( width );
	glBegin( GL_LINE_STRIP );
	for ( float t = 0; t < 1+maxStepsM1; t += maxStepsM1 ) {
		vPoint = pointOnCurve( controlPts, ptsCounter, t );
		glVertex3f( vPoint.x, vPoint.y, vPoint.z );
	}
	glEnd();
}
////////////////////////////////////////////////////////////////////////////////
/// Procedura rysujaca szescian.
////////////////////////////////////////////////////////////////////////////////

///                                                                        
///             (H)              (G)                                       
///              ,O------------;O                                          
///            ,* |          ,* |                                          
///          O*---+--------O`   |                                          
///       (D)|    |        |(C) |                                          
///          |    |        |    |                                          
///          |    |        |    |                                          
///          |   ,O--------+---;O                                          
///          | ,* (E)      | ,*  (F)                                      
///          O*------------O`                                         
///       (A)              (B)                                                 
///                                                                        
/// Kolejnosc "stawiania" punktow jest wazna, gdyz zalezy od niej poprawne teksturowanie.
/// Ta procedura nie przewiduje teksturowania, ale jest dobrym miejscem, aby o tym wspomniec.

void drawCube( float x, float y, float z, float sizeX, float sizeY, float sizeZ ) {
	float sizeX2 = sizeX / 2;
	float sizeY2 = sizeY / 2;
	float sizeZ2 = sizeZ / 2;
	glPushMatrix();
	glTranslatef( x, y, z );
	glBegin( GL_QUADS );
	glNormal3f( 0, 0, 1 );
	glVertex3f(  - sizeX2,  - sizeY2, sizeZ2 ); // A // bottom left
	glVertex3f( sizeX2,  - sizeY2, sizeZ2 ); // B // bottom right
	glVertex3f( sizeX2, sizeY2, sizeZ2 ); // C // top right
	glVertex3f(  - sizeX2, sizeY2, sizeZ2 ); // D // top left

	glNormal3f( 1, 0, 0 );
	glVertex3f( sizeX2,  - sizeY2, sizeZ2 ); // B // left down
	glVertex3f( sizeX2,  - sizeY2,  - sizeZ2 ); // F // right down
	glVertex3f( sizeX2, sizeY2,  - sizeZ2 ); // G // right up
	glVertex3f( sizeX2, sizeY2, sizeZ2 ); // C // left up

	glNormal3f( 0, 0,  - 1 );
	glVertex3f(  - sizeX2,  - sizeY2,  - sizeZ2 ); // E // left down
	glVertex3f( sizeX2,  - sizeY2,  - sizeZ2 ); // F // right down
	glVertex3f( sizeX2, sizeY2,  - sizeZ2 ); // G // right up
	glVertex3f(  - sizeX2, sizeY2,  - sizeZ2 ); // H // left up

	glNormal3f(  - 1, 0, 0 );
	glVertex3f(  - sizeX2,  - sizeY2,  - sizeZ2 ); // E // right down
    glVertex3f(  - sizeX2,  - sizeY2, sizeZ2 ); // A // left down
	glVertex3f(  - sizeX2, sizeY2, sizeZ2 ); // D // left up
    glVertex3f(  - sizeX2, sizeY2,  - sizeZ2 ); // H // right up
    
	glNormal3f( 0, 1, 0 );
	glVertex3f(  - sizeX2, sizeY2, sizeZ2 ); // D // left down
	glVertex3f( sizeX2, sizeY2, sizeZ2 ); // C // right down
	glVertex3f( sizeX2, sizeY2,  - sizeZ2 ); // G // right up
	glVertex3f(  - sizeX2, sizeY2,  - sizeZ2 ); // H // left up

	glNormal3f( 0,  - 1, 0 );
	glVertex3f(  - sizeX2,  - sizeY2, sizeZ2 ); // A // left down
	glVertex3f( sizeX2,  - sizeY2, sizeZ2 ); // B // right down
	glVertex3f( sizeX2,  - sizeY2,  - sizeZ2 ); // F // right up
	glVertex3f(  - sizeX2,  - sizeY2,  - sizeZ2 ); // E // left up
	glEnd();
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////////////////
void cChessboard::show() {
	drawBoard();
	drawPawns();
}
////////////////////////////////////////////////////////////////////////////////

/// Zaleznie od pozycji myszy, funkcja rysuje odpowiednio pokolorowane pola.
void cChessboard::drawBoard() {
	glPushMatrix();
	glTranslatef( x - 4, y - 4, z );
	//robplansze();
	glCallList( pedestal );
	//glRotatef(90,1,0,0);
	int Xi = 0, Xj = 0, name = 0;
	float Xz = 0.2f;
	for ( int i = 0; i < 8; i++ ) {
		for ( int j = 0; j < 8; j++ ) {
			if ( i % 2 == j % 2 ) {
				glColor3f( 0.3, 0.3, 0.3 );
				if ( myField[name]->underAttack ) {
					glColor3f( 0.45, 0.1, 0.1 );
				}
				if ( theObject == name ) {
					Xi = i;
					Xj = j;
					glColor3f( 0.1, 0.45, 0.1 );
					if ( myField[name]->underAttack ) {
						glColor3f( 0.45, 0.45, 0.1 );
					}
				}
			} else {
				glColor3f( 0.7, 0.7, 0.7 );
			}

			glPushMatrix();
			glTranslatef( j, i, Xz + 1 );
			glCallList( square );
			glPopMatrix();
			if ( i % 2 == j % 2 ) {
				name++;
			}
		}
		if ( i % 2 == 0 ) {
			name++;
		}

	}
	if ( Xi + Xj >= 1 && Xi + Xj <= 16 ) {
		Xz = 0.2f;
		glLineWidth( 2.0 );
		glColor3f( 0, 0.3, 0.1 );
		float g = 0.0;
		glBegin( GL_LINE_LOOP );
		glVertex3f( Xj + 1.0 + g, Xi + 1.0 + g, Xz + 1.0f ); // Top Right Of The Quad (Front)
		glVertex3f( Xj - g, Xi + 1.0 + g, Xz + 1.0f ); // Top Left Of The Quad (Front)
		glVertex3f( Xj - g, Xi - g, Xz + 1.0f ); // Bottom Left Of The Quad (Front)
		glVertex3f( Xj + 1.0 + g, Xi - g, Xz + 1.0f );
		glEnd();
		glEnd();
		glLineWidth( 1.0 );
	}
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////////////////
void cChessboard::drawBoardHT() {
	glPushMatrix();
	glTranslatef( x - 4, y - 4, z );
	float zz = 0.2f;
	glBegin( GL_QUADS );
	glVertex3f( 1.0f, 9.0f, zz + 1.0f );
	glVertex3f( 9.0f, 9.0f, zz + 1.0f );
	glVertex3f( 9.0f, 9.0f,  - 10.0f );
	glVertex3f( 1.0f, 9.0f,  - 10.0f );

	glVertex3f( 1.0f, 1.0f, zz + 1.0f );
	glVertex3f( 9.0f, 1.0f, zz + 1.0f );
	glVertex3f( 9.0f, 1.0f,  - 10.0f );
	glVertex3f( 1.0f, 1.0f,  - 10.0f );

	glVertex3f( 1.0f, 1.0f, zz + 1.0f );
	glVertex3f( 1.0f, 1.0f,  - 10.0f );
	glVertex3f( 1.0f, 9.0f,  - 10.0f );
	glVertex3f( 1.0f, 9.0f, zz + 1.0f );

	glVertex3f( 9.0f, 1.0f, zz + 1.0f );
	glVertex3f( 9.0f, 1.0f,  - 10.0f );
	glVertex3f( 9.0f, 9.0f,  - 10.0f );
	glVertex3f( 9.0f, 9.0f, zz + 1.0f );

	glVertex3f(  - 10,  - 10, 0 );
	glVertex3f( 10,  - 10, 0 );
	glVertex3f( 10, 10, 0 );
	glVertex3f(  - 10, 10, 0 );
	glEnd();
	int name = 0;
	for ( int i = 0; i < 8; i++ ) {
		for ( int j = 0; j < 8; j++ ) {
			if ( i % 2 == j % 2 ) {
				glPushName( name );
				name++;
				glBegin( GL_QUADS );
				glNormal3f( 0, 0, 1 );
				glVertex3f( j + ( 0.0f ), i + ( 0.0f ), zz + 1.0f ); // Bottom Left (0,0)
				glVertex3f( j + 1.0, i + ( 0.0f ), zz + 1.0f ); // Bottom Right
				glVertex3f( j + 1.0, i + ( 1.0 ), zz + 1.0f ); // Top Right
				glVertex3f( j + ( 0.0f ), i + ( 1.0 ), zz + 1.0f ); // Top Left (0,1)
				glEnd();
				glPopName();
			} //else{  };
		}
		if ( i % 2 == 0 ) {
			name++;
		}
	}
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////////////////
cPawn::cPawn( int XbORw ) {
	bORw = XbORw;

	switch ( bORw ) {
		case 1:
			if ( pawnDL = initDL( drawFuturisticPawnWhite )) {
				break;
			}
		case 0:
			if ( pawnDL = initDL( drawFuturisticPawnBlack )) {
				break;
			}
		default:
			break;
	}
	queen = false;
	rotY = rand() % 360;
	selectedPawnDL = initDL( drawFuturisticPawnSelected );
// Uzywane przy debugowaniu.
//	if ( bORw == 1 ) {
//		 myLog << "Created: a white pawn.\n" ;
//	} else {
//		 myLog << "Created: a black pawn.\n" ;
//	}
}
////////////////////////////////////////////////////////////////////////////////
cPawn::~cPawn() {
// Uzywane przy debugowaniu.
//    switch(bORw){
//        case 1 :
//            myLog << "Deleted a white pawn.\n" ;
//            break;
//        case 0 :
//            myLog << "Deleted a black pawn.\n" ;
//            break;
//        default :
//            break;
//    }

}
////////////////////////////////////////////////////////////////////////////////
/// Rysowanie zaznaczenia wybranego pionka.
////////////////////////////////////////////////////////////////////////////////
void markPawn() {
	static int rotY = 0;
	glPushMatrix();
	glTranslatef( 0, 0, 0.7 );
	glRotatef( 15, 1, 0, 0 );
	glColor3f( 1, 0, 0 );
	rotY -= 10;
	for ( int Xy = 0; Xy < 4; Xy++ ) {
		glPushMatrix();
		glRotatef( rotY + ( 90 *Xy ), 0, 0, 1 );

		drawSphere( 0, 0.3, 0, 0.05, 15 );
		glRotatef( 11, 0, 0, 1 );
		drawSphere( 0, 0.3, 0, 0.02, 15 );
		glRotatef( 13, 0, 0, 1 );
		drawSphere( 0, 0.3, 0, 0.015, 10 );
		glRotatef( 15, 0, 0, 1 );
		drawSphere( 0, 0.3, 0, 0.01, 10 );
		glRotatef( 17, 0, 0, 1 );
		drawSphere( 0, 0.3, 0, 0.01, 10 );
		glEnable( GL_BLEND );
		glBlendFunc( GL_ONE, GL_ONE );
		drawSphere( 0, 0.3, 0, 0.015, 10 );
		glRotatef(  - 17, 0, 0, 1 );
		drawSphere( 0, 0.3, 0, 0.015, 10 );
		glRotatef(  - 15, 0, 0, 1 );
		drawSphere( 0, 0.3, 0, 0.02, 15 );
		glRotatef(  - 13, 0, 0, 1 );
		drawSphere( 0, 0.3, 0, 0.025, 15 );
		glRotatef(  - 11, 0, 0, 1 );
		drawSphere( 0, 0.3, 0, 0.055, 15 );
		glDisable( GL_BLEND );
		glPopMatrix();
	}
	glPopMatrix();
}

///////////////////////////////////////////////////////////////////////////
/// Zaznacza krolowa.
///////////////////////////////////////////////////////////////////////////

void markQueen() {
	glTranslatef( 0, 0, 0.7 );
	drawCube( 0, 0, 0, 0.35, 0.35, 0.35 );
}
////////////////////////////////////////////////////////////////////////////////
void cPawn::draw( bool selected, int XxBoard ) {
	int tmpXxBoard = XxBoard;
	if ( XxBoard > 4 ) {
		tmpXxBoard--;
	}
	if ( XxBoard > 13 ) {
		tmpXxBoard--;
	}
	if ( XxBoard > 22 ) {
		tmpXxBoard--;
	}
	if ( XxBoard > 31 ) {
		tmpXxBoard--;
	}
	glPushMatrix();
	glTranslatef(( tmpXxBoard % 4 ) *2+( int( tmpXxBoard / 4 ) % 2 ) + 0.5, int( tmpXxBoard / 4 ) + 0.5, 0 );
	glCallList( pawnDL );
	if ( selected ) {
		markPawn();
		//glCallList( selectedPawnDL );
	}
	if ( queen ) {
		markQueen();
	}
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////////////////
cField::cField() {
	selected = false;
	occupied = false;
	underAttack = false;
	zbity = false;
	occupiedBy = 0;
//    myLog << "Created an anonymous field.\n" ;
}
////////////////////////////////////////////////////////////////////////////////
cField::cField( int XxBoard ) {
	xBoard = XxBoard;
	selected = false;
	occupied = false;
	underAttack = false;
	zbity = false;
	occupiedBy = 0;
//    myLog << "Created a field of No. " << XxBoard << " (empty).\n" ;
}
////////////////////////////////////////////////////////////////////////////////
cField::cField( int XbORw, int XxBoard ) {
	selected = false;
	occupied = true;
	underAttack = false;
	zbity = false;
	occupiedBy = new cPawn( XbORw );
	xBoard = XxBoard;
//	if ( XbORw == 1 ) {
//		myLog << "Created a field of No. " << XxBoard << " (occupied by a white pawn).\n";
//	} else {
//		myLog << "Created a field of No. " << XxBoard << " (occupied by black pawn).\n";
//	}
}
////////////////////////////////////////////////////////////////////////////////
cField::~cField() {
	int ggg = 909;
	if ( occupied ) {
		delete occupiedBy;
	}
	//  myLog << "Deleted a field of No. " << xBoard << "\n" ;
	//delete occupiedBy;
}
////////////////////////////////////////////////////////////////////////////////
void cField::occupy( cPawn *by ) {
	occupied = true;
	selected = false;
	occupiedBy = by;
}
////////////////////////////////////////////////////////////////////////////////
cPawn *cField::deoccupy() {
	occupied = false;
	selected = false;
	return occupiedBy;
}
////////////////////////////////////////////////////////////////////////////////
void cField::select() {
	selected = true;
}
////////////////////////////////////////////////////////////////////////////////
void cField::threaten() {
	underAttack = true;
}
////////////////////////////////////////////////////////////////////////////////
void cField::zbij() {
	zbity = true;
}
////////////////////////////////////////////////////////////////////////////////
void cField::deselect() {
	selected = false;
}
////////////////////////////////////////////////////////////////////////////////
void cField::relieve() {
	underAttack = false;
}
////////////////////////////////////////////////////////////////////////////////
void cField::zaznacz() {
	zaznaczony = true;
}
////////////////////////////////////////////////////////////////////////////////
void cField::odznacz() {
	zaznaczony = false;
}
////////////////////////////////////////////////////////////////////////////////
void cField::postaw() {
	zbity = false;
}
////////////////////////////////////////////////////////////////////////////////
void cField::crownPawn() {
	occupiedBy->queen = true;
}
////////////////////////////////////////////////////////////////////////////////
void cField::displayDirections() {
	glColor3f( 1, 1, 1 );
	glLineWidth( 10 );
	switch ( occupiedBy->bORw ) {
		case 1:
			break;
		case 0:
			break;
		default:
			break;
	}
}
////////////////////////////////////////////////////////////////////////////////
void cChessboard::drawPawns() {
	glPushMatrix();
	glTranslatef( x - 4, y - 4, 1.2 );
	for ( int i = 0; i < 36; i++ ) {
		if ( myField[i] != 0 && ( myField[i]->occupied )) {
			myField[i]->drawPawn();
		}
	}
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////////////////
void cField::drawPawn() {
	occupiedBy->draw( selected, xBoard );
}
////////////////////////////////////////////////////////////////////////////////
cChessboard::cChessboard( float Xx, float Xy, float Xz ) {
	x = Xx;
	y = Xy;
	z = Xz;
	myLog << "Stworzono szachownice:" << "\n\t x -> " << Xx << "\n\t y -> " << Xy << "\n\t z -> " << Xz << "\n";
}
////////////////////////////////////////////////////////////////////////////////
void cChessboard::initBoard() {
	myLog << "Inicjalizowanie szachownicy..." ;
	int i = 0;
	while ( i <= 35 ) {
		if ( i != 4 && i != 13 && i != 22 && i != 31 ) {
			if ( i < 13 ) {
				myField[i] = new cField( 1, i );
			} else {
				if ( i > 22 ) {
					myField[i] = new cField( 0, i );
				} else {
					myField[i] = new cField( i );
				}
			}
		} else {
			myField[i] = 0;
		}
		i++;
	}
	selectedField =  - 1;
	whiteToMove = true;
	myLog << "OK\n" ;
}
////////////////////////////////////////////////////////////////////////////////
cChessboard::~cChessboard() {
	for ( int i = 0; i < 36; i++ ) {
		delete myField[i];
	}
	myLog << "Zniszczono szachownice...\n";
}
////////////////////////////////////////////////////////////////////////////////
void cChessboard::reInitBoard() {
	myLog << "Reinicjalizowanie szachownicy.\n";
	for ( int i = 0; i < 36; i++ ) {
		if ( i != 4 && i != 13 && i != 22 && i != 31 ) {
			delete myField[i];
		}
	}
	int i = 0;
	while ( i <= 35 ) {
		if ( i != 4 && i != 13 && i != 22 && i != 31 ) {
			if ( i < 13 ) {
				myField[i] = new cField( 1, i );
			} else {
				if ( i > 22 ) {
					myField[i] = new cField( 0, i );
				} else {
					myField[i] = new cField( i );
				}
			}
		} else {
			myField[i] = 0;
		}
		i++;
	}
	selectedField =  - 1;
	whiteToMove = true;
}
////////////////////////////////////////////////////////////////////////////////
/// Rysuje pole szachownicy.
////////////////////////////////////////////////////////////////////////////////
void drawSquare() {
	if ( !noTexturing ) {
		glBindTexture( GL_TEXTURE_2D, textureList[1] );
	}
	glBegin( GL_QUADS );

	glTexCoord2f( 0.0f, 0.0f );
	glVertex3f( 0.0f, 0.0f, 0.0f ); // Bottom Left (0,0)

	glTexCoord2f( 1.0f, 0.0f );
	glVertex3f( 1.0f, 0.0f, 0.0f ); // Bottom Right

	glTexCoord2f( 1.0f, 1.0f );
	glVertex3f( 1.0f, 1.0f, 0.0f ); // Top Right

	glTexCoord2f( 0.0f, 1.0f );
	glVertex3f( 0.0f, 1.0f, 0.0f ); // Top Left (0,1)
	glEnd();
}
////////////////////////////////////////////////////////////////////////////////
/// Rysuje podstawe szachownicy.
////////////////////////////////////////////////////////////////////////////////
void drawPedestal() {
	glColor3f( 0.4, 0.2, 0.2 );
	if ( !noTexturing ) {
		glBindTexture( GL_TEXTURE_2D, textureList[3] );
	}
	glBegin( GL_QUADS ); // front, right under marble
	glNormal3f( 0, 0, 1 );
	glTexCoord2f( 0.0f, 0.0f );
	glVertex3f(  - 1.4f,  - 1.4f, 1 ); // Bottom Left (0,0)
	glTexCoord2f( 13.0f, 0.0f );
	glVertex3f( 9.4f,  - 1.4f, 1 ); // Bottom Right (1,0)
	glTexCoord2f( 13.0f, 13.0f );
	glVertex3f( 9.4f, 9.4f, 1 ); // Top Right (1,1)
	glTexCoord2f( 0.0f, 13.0f );
	glVertex3f(  - 1.4f, 9.4f, 1 ); // Top Left (0,1)
	glEnd();
	if ( !noTexturing ) {
		glBindTexture( GL_TEXTURE_2D, textureList[3] );
	}
	glBegin( GL_QUADS ); // above the above
	glNormal3f( 0, 1, 0 );
	glTexCoord2f( 0.0f, 0.0f );
	glVertex3f(  - 1.4f, 9.4f, 1 );
	glTexCoord2f( 13.0f, 0.0f );
	glVertex3f( 9.4f, 9.4f, 1 );
	glTexCoord2f( 13.0f, 3.61f );
	glVertex3f( 9.4f, 9.4f, 0 );
	glTexCoord2f( 0.0f, 3.61f );
	glVertex3f(  - 1.4f, 9.4f, 0 );
	glEnd();
	if ( !noTexturing ) {
		glBindTexture( GL_TEXTURE_2D, textureList[3] );
	}
	glBegin( GL_QUADS ); //  under the front
	glNormal3f( 0, 0, 0 );
	glTexCoord2f( 0.0f, 0.0f );
	glVertex3f(  - 1.4f,  - 1.4f, 1 );
	glTexCoord2f( 13.0f, 0.0f );
	glVertex3f( 9.4f,  - 1.4f, 1 );
	glTexCoord2f( 13.0f, 3.61f );
	glVertex3f( 9.4f,  - 1.4f, 0 );
	glTexCoord2f( 0.0f, 3.61f );
	glVertex3f(  - 1.4f,  - 1.4f, 0 );
	glEnd();
	glBegin( GL_QUADS ); // left panel
	glNormal3f( 0, 0, 0 );
	glTexCoord2f( 0.0f, 0.0f );
	glVertex3f(  - 1.4f,  - 1.4f, 0 );
	glTexCoord2f( 3.61f, 0.0f );
	glVertex3f(  - 1.4f,  - 1.4f, 1 );
	glTexCoord2f( 3.61f, 13.0f );
	glVertex3f(  - 1.4f, 9.4f, 1 );
	glTexCoord2f( 0.0f, 13.0f );
	glVertex3f(  - 1.4f, 9.4f, 0 );
	glEnd();
	glBegin( GL_QUADS ); // right panel
	glNormal3f( 1, 0, 0 );
	glTexCoord2f( 0.0f, 0.0f );
	glVertex3f( 9.4f,  - 1.4f, 0 );
	glTexCoord2f( 3.61f, 0.0f );
	glVertex3f( 9.4f,  - 1.4f, 1 );
	glTexCoord2f( 3.61f, 13.0f );
	glVertex3f( 9.4f, 9.4f, 1 );
	glTexCoord2f( 0.0f, 13.0f );
	glVertex3f( 9.4f, 9.4f, 0 );
	glEnd();
	glBegin( GL_QUADS ); // backside
	glNormal3f( 0, 0, 0 );
	glTexCoord2f( 0.0f, 0.0f );
	glVertex3f(  - 1.4f,  - 1.4f, 0 ); // Bottom Left (0,0)
	glTexCoord2f( 13.0f, 0.0f );
	glVertex3f( 9.4f,  - 1.4f, 0 ); // Bottom Right (1,0)
	glTexCoord2f( 13.0f, 13.0f );
	glVertex3f( 9.4f, 9.4f, 0 ); // Top Right (1,1)
	glTexCoord2f( 0.0f, 13.0f );
	glVertex3f(  - 1.4f, 9.4f, 0 ); // Top Left (0,1)
	glEnd();

	glBegin( GL_QUADS );
	float zz = 1.2;
	glColor3f( 0.1, 0.1, 0.3 );
	glNormal3f( 0, 1, 0 );
	glVertex3f( 0.0f, 8.0f, zz );
	glVertex3f( 8.0f, 8.0f, zz );
	glVertex3f( 8.0f, 8.0f, 0.5f );
	glVertex3f( 0.0f, 8.0f, 0.5f );
	glNormal3f( 0,  - 1, 0 );
	glVertex3f( 0.0f, 0.0f, zz );
	glVertex3f( 8.0f, 0.0f, zz );
	glVertex3f( 8.0f, 0.0f, 0.5f );
	glVertex3f( 0.0f, 0.0f, 0.5f );
	glNormal3f(  - 1, 0, 0 );
	glVertex3f( 0.0f, 0.0f, zz );
	glVertex3f( 0.0f, 0.0f, 0.5f );
	glVertex3f( 0.0f, 8.0f, 0.5f );
	glVertex3f( 0.0f, 8.0f, zz );
	glNormal3f( 1, 0, 0 );
	glVertex3f( 8.0f, 0.0f, zz );
	glVertex3f( 8.0f, 0.0f, 0.5f );
	glVertex3f( 8.0f, 8.0f, 0.5f );
	glVertex3f( 8.0f, 8.0f, zz );

	glEnd();
}
////////////////////////////////////////////////////////////////////////////////
/// Oblicza kolejne punkty krzywej Baziera.
////////////////////////////////////////////////////////////////////////////////
cVector3f Bazier::pointOnCurve( cVector3f *point, int ptsCounter, float t ) {
	int i;
	float var, var2, var3;
	cVector3f currentPoint( 0.0f, 0.0f, 0.0f );
	int *pscTable = new int[ptsCounter];

	var = 1-t;
	for ( int g = 0; g < ptsCounter; g++ ) {
		pscTable[g] = Pascal( ptsCounter - 1, g );
	}
	for ( i = 0; i < ptsCounter; i++ ) {
		currentPoint.x = currentPoint.x + ( pow( var, i ) *pow( t, ( ptsCounter - 1 ) - i ) *pscTable[i] *point[i].x );
		currentPoint.y = currentPoint.y + ( pow( var, i ) *pow( t, ( ptsCounter - 1 ) - i ) *pscTable[i] *point[i].y );
		currentPoint.z = currentPoint.z + ( pow( var, i ) *pow( t, ( ptsCounter - 1 ) - i ) *pscTable[i] *point[i].z );
	}

	delete pscTable;

	return currentPoint;
}
////////////////////////////////////////////////////////////////////////////////
/// Oblicza wartosc liczby z trojkata Pascala o zadanych wspolrzednych.
////////////////////////////////////////////////////////////////////////////////
int Pascal( int pscRow, int pscCol ) {
	if ( pscRow > pscCol && pscCol > 0 && ( pscCol != 0 && pscRow != pscCol )) {
		return ( Pascal( pscRow - 1, pscCol - 1 ) + Pascal( pscRow - 1, pscCol ));
	} else {
		return 1;
	}
}
////////////////////////////////////////////////////////////////////////////////
/// Rysuje kule.
////////////////////////////////////////////////////////////////////////////////
void drawSphere( float x, float y, float z, double radius, int precision ) {
	GLUquadricObj *pSphere = gluNewQuadric();
	glPushMatrix();
	glTranslatef( x, y, z );
	gluSphere( pSphere, radius, precision, precision );
	glPopMatrix();
	gluDeleteQuadric( pSphere );
}
////////////////////////////////////////////////////////////////////////////////
/// Rysuje bialy pionek.
////////////////////////////////////////////////////////////////////////////////
void drawFuturisticPawnBlack() {
	cVector3f tmpB[] =  {
		cVector3f( 0, 0.1, 0 ), cVector3f( 1, 0.2, 1 ), cVector3f(  - 1, 0.4,  - 0.2 ), cVector3f( 0, 0.5, 0 )
	};
	Bazier bcc( 4, tmpB );
	glPushMatrix();
	glRotatef( 90, 1, 0, 0 );
	glColor3f( 0.1, 0.1, 0.1 );
	drawCube( 0, 0.05, 0, 0.4, 0.1, 0.4 );
	glColor3f( 0.3, 0.3, 0.3 );
	glRotatef( rand() % 90, 0, 1, 0 );
	bcc.draw( 2.0, 15 );
	drawSphere( 0, 0.7, 0, 0.2, 20 );
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////////////////
/// Rysuje czarny pionek.
////////////////////////////////////////////////////////////////////////////////
void drawFuturisticPawnWhite() {
	cVector3f tmpB[] =  {
		cVector3f( 0, 0.1, 0 ), cVector3f( 1, 0.2, 1 ), cVector3f(  - 1, 0.4,  - 0.2 ), cVector3f( 0, 0.5, 0 )
	};
	Bazier bcc( 4, tmpB );
	glPushMatrix();
	glRotatef( 90, 1, 0, 0 );
	glColor3f( 0.5, 0.5, 0.5 );
	drawCube( 0, 0.05, 0, 0.4, 0.1, 0.4 );
	glColor3f( 0.7, 0.7, 0.7 );
	glRotatef( rand() % 90, 0, 1, 0 );
	bcc.draw( 2.0, 15 );
	drawSphere( 0, 0.7, 0, 0.2, 20 );
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////////////////
/// Rysuje zaznaczenie pionka.
////////////////////////////////////////////////////////////////////////////////
void drawFuturisticPawnSelected() {
	glEnable( GL_BLEND );
	glBlendFunc( GL_ONE, GL_ONE );
	glColor3f( 1, 0.3, 0.3 );
	drawCube( 0, 0, 0.5, 1, 1, 1 );
	glDisable( GL_BLEND );
	glColor3f( 0.7, 0.0, 0.0 );
	glLineWidth( 1.5 );
	glBegin( GL_LINE_STRIP );
	glVertex3f( 0.5, 0.5, 0 );
	glVertex3f( 0.5, 0.5, 1 );
	glEnd();
	glBegin( GL_LINE_STRIP );
	glVertex3f(  - 0.5,  - 0.5, 0 );
	glVertex3f(  - 0.5,  - 0.5, 1 );
	glEnd();
	glBegin( GL_LINE_STRIP );
	glVertex3f(  - 0.5, 0.5, 0 );
	glVertex3f(  - 0.5, 0.5, 1 );
	glEnd();
	glBegin( GL_LINE_STRIP );
	glVertex3f( 0.5,  - 0.5, 0 );
	glVertex3f( 0.5,  - 0.5, 1 );
	glEnd();
	glBegin( GL_LINE_LOOP );
	glVertex3f(  - 0.5,  - 0.5, 1 );
	glVertex3f( 0.5,  - 0.5, 1 );
	glVertex3f( 0.5, 0.5, 1 );
	glVertex3f(  - 0.5, 0.5, 1 );
	glEnd();
	glBegin( GL_LINE_LOOP );
	glVertex3f(  - 0.5,  - 0.5, 0 );
	glVertex3f( 0.5,  - 0.5, 0 );
	glVertex3f( 0.5, 0.5, 0 );
	glVertex3f(  - 0.5, 0.5, 0 );
	glEnd();
}
////////////////////////////////////////////////////////////////////////////////

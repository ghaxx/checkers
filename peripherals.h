#ifndef _PERIPHERALS_H
#define _PERIPHERALS_H
////////////////////////////////////////////////////////////////////////////////
/// Klasa klawiatury. 
////////////////////////////////////////////////////////////////////////////////

/// Przechowuje wiadomosci, ktory klawisz jest aktualnie wcisniety.
class cKeyboard {
	public:
		cKeyboard();
		/// Wcisniete klawisze.
		bool fPressed, upPressed, downPressed, leftPressed, rightPressed, 
             wPressed, sPressed,  aPressed,    dPressed, 
             qPressed, ePressed, 
             spacePressed;
		/// Co zrobic, jak ktos nacisnie klawisz...
		void handleKeyPressEvent( SDL_keysym *keysym );
		/// Co zrobic, jak ktos juz przestanie dusic ten klawisz...
		void handleKeyReleaseEvent( SDL_keysym *keysym );
};
////////////////////////////////////////////////////////////////////////////////
/// Klasa myszy. 
////////////////////////////////////////////////////////////////////////////////

/// Przechowuje dane o myszce.
class cMouse {
	public:
		cMouse();
		/// Opisuje akcje, ktore nalezy przedsiewziac, zaleznie od naciscnietych przyciskow.
		void handleMouseDownEvent( SDL_MouseButtonEvent *button );
		/// Mowi, co zrobic, kiedy jakis przycisk zostanie puszczony.
		void handleMouseUpEvent( SDL_MouseButtonEvent *button );
		/// Aktualizuje dane.
		void update();
		/// Zwraca pozycje myszy, a konkretnie jej wspolrzedna pozioma.
		int getX();
		/// Zwraca wspolrzedna pionowa myszy.
		int getY();
		/// Zwraca ID obiektu kliknietego podczas projekcji poprzedniej sceny.
		int getClicked();
		/// Zwraca ID obiektu ostatnio kliknietego.
		int getSelected();
		/// Zmienne opisujace stan przyciskow myszy (true - przysick wcisniety).
		bool mouseDown, leftDown, rightDown, middleDown;
	private:
	    /// Wspolrzedne myszy.
		int x, y;
		/// Te zmienne przechowuja ID obiektow, ktore zostaly klikniete.
		int hitDown, hitUp, 
            selectedObject,  /// ID ostatnio kliknietego obiektu.
            clickedObject;   /// ID obiektu kliknieto w ostatniej scenie.
};
////////////////////////////////////////////////////////////////////////////////
extern cMouse myMouse;
extern cKeyboard myKeyboard;

#endif // _PERIPHERALS_H

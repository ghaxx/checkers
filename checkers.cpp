#include "init.h"
#include "camera.h"
#include "drawings.h"
#include "textures.h"
#include "peripherals.h"
#include "menu.h"

int liczbabic;
int liczbaznalezionych=0;
int kierunek;
int skad;
int maks;
//-------------------------TU SA MOJE DEFINICJE---------------------------------

int cChessboard::coJestNaPolu(int pole)
     {   
         if (pole<36)
         {
          if (myField[pole]!=0) 
                    {
                        if (myField[pole]->occupied)
                            { 
                                if (myField[pole]->occupiedBy->bORw)
                                    {
                                        if (myField[pole]->occupiedBy->queen)
                                             return 12; 
                                        else  return 11;    
                                    }
                                else 
                                    {
                                        if (myField[pole]->occupiedBy->queen)
                                             return 2;
                                        else  return 1;                                       
                                    }        
                            } 
                        else return 10;       
                    } else return 0;    
         } else  return 0;
     }    
//====================================================================
bool cChessboard::ifMovePossible(int from, int destination)/// test doxygena
    {        
       
     int F = from;                                            // 0 - down
     int D = destination;
     int F2=F;
     int D2=D;
     int czym = coJestNaPolu(F);
     int gdzie = coJestNaPolu(D);
     int i;
     int ile;
     int max1;
     int max0;
     bool juz=false;
     bool possible = false;
     if (whiteToMove)  max1=maxBic(1);
     if (!whiteToMove) max0=maxBic(0);
     if (czym>10 && gdzie==10&&whiteToMove)//sprawdza zwyczajny ruch dla bialych
      { 
      if((D-F==4  ||  D-F== 5)
         && (D != 4 && D != 13 && D!= 22 && D != 31) &&max1==0
         && ileZagrozonych()==0)
         {possible = true;}  
      if (czym==11) 
          {    
               if (coJestNaPolu(F+(D-F)/2)<10 && coJestNaPolu(D)==10// pojedyncze bicie pionka
                   && (D-F==8 || D-F==-8 ||D-F==10 || D-F==-10)
                   && jestZagrozony(F+(D-F)/2,1)&&max1==1) possible=true; 
               //if (jestBicie(1,D)) {possible=false; }
          }
      if (czym==12&&max1==0&&ileZagrozonych()==0) //sprawdza ruch damki
          {  
                      if (abs(D-F)%5==0&&D<F&&czteryCzyPiec(5,F,D)==5)
                        {   
                            possible=true;
                            for (i=1;i<((F-D)/5)+1;i++)
                                 {
                                     if (coJestNaPolu(F-i*5)!=10){possible=false;}
                                 }
                        } 
                      if (abs(D-F)%5==0&&D>F&&czteryCzyPiec(5,F,D)==5)
                        {   
                            possible=true;
                            for (i=1;i<((D-F)/5)+1;i++)
                                 {
                                     if (coJestNaPolu(F+i*5)!=10){possible=false;}
                                 }
                                 
                        } 
                      if (abs(D-F)%4==0&&D<F&&czteryCzyPiec(4,F,D)==4)
                        {   
                            possible=true;
                            for (i=1;i<((F-D)/4)+1;i++)
                                 {
                                     if (coJestNaPolu(F-i*4)!=10){possible=false;}
                                 }
                        } 
                      if (abs(D-F)%4==0&&D>F&&czteryCzyPiec(4,F,D)==4)
                        {
                            possible=true;
                            for (i=1;i<((D-F)/4)+1;i++)
                                 {
                                     if (coJestNaPolu(F+i*4)!=10){possible=false;}
                                 }
                        }           
          }    
         if (czym==12&&max1==1&&ileZagrozonych()==1&&max1==1) //bicia damki bialej    
         {                
                  if (abs(D-F)%5==0&&D<F&&czteryCzyPiec(5,F,D)==5)
                        {   
                            possible=true;
                            i=0;
                          while (coJestNaPolu(F-(i+1)*5)==10)
                                 {
                                      i++;     
                                 }       
                                     
                                     if (coJestNaPolu(F-(i+1)*5)>2||coJestNaPolu(F-(i+1)*5)==0) {possible=false;}
                                     if ((F-(i+1)*5)<D) {possible=false;}
                                    
                 
                          i=i+2;          
                          while ((F-i*5)!=D)
                                 {
                                   if (coJestNaPolu(F-i*5)!=10) {possible=false;}
                                   i++;
                                 }   
                        } 
                      if (abs(D-F)%5==0&&D>F&&czteryCzyPiec(5,F,D)==5)
                        {   
                            possible=true;
                            i=0;
                            while (coJestNaPolu(F+(i+1)*5)==10)
                                 {
                                      i++;     
                                 }       
                                     
                                     if (coJestNaPolu(F+(i+1)*5)>2||coJestNaPolu(F+(i+1)*5)==0) {possible=false;}
                                     if ((F+(i+1)*5)>D) {possible=false;}
                                    
                         
                          i=i+2;          
                          while ((F+i*5)!=D)
                                 {
                                   if (coJestNaPolu(F+i*5)!=10) {possible=false;}
                                   i++;
                                 }                
                            
                        } 
                     if (abs(D-F)%4==0&&D<F&&czteryCzyPiec(4,F,D)==4)
                        {   
                            possible=true;
                            i=0;
                          while (coJestNaPolu(F-(i+1)*4)==10)
                                 {
                                      i++;     
                                 }       
                                     
                                     if (coJestNaPolu(F-(i+1)*4)>2||coJestNaPolu(F-(i+1)*4)==0) {possible=false;}
                                     if ((F-(i+1)*4)<D) {possible=false;}
                                    
                         
                          i=i+2;          
                          while ((F-i*4)!=D)
                                 {
                                   if (coJestNaPolu(F-i*4)!=10) {possible=false;}
                                   i++;
                                 }                                             
                        } 
                      if (abs(D-F)%4==0&&D>F&&czteryCzyPiec(4,F,D)==4)
                        {    
                            possible=true;
                            i=0;
                            while (coJestNaPolu(F+(i+1)*4)==10)
                                 {
                                      i++;     
                                 }       
                                     
                                     if (coJestNaPolu(F+(i+1)*4)>2||coJestNaPolu(F+(i+1)*4)==0) {possible=false;}
                                     if ((F+(i+1)*4)>D) {possible=false;}
                                    
                          i=i+2;          
                          while ((F+i*4)!=D)
                                 {
                                   if (coJestNaPolu(F+i*4)!=10) {possible=false;}
                                   i++;
                                 }                
                            
                        }    
                         
         }    
        
      }    
     if (czym<10 && gdzie==10&& !whiteToMove) //sprawdza zwyczajny ruch dla czarnych
      { 
      if((F-D==4  ||  F-D== 5)
         && (D != 4 && D != 13 && D!= 22 && D != 31) && max0==0
         && ileZagrozonych()==0 )
         {possible = true;}   
      if (coJestNaPolu(D)!=10) possible=false;  
      if (coJestNaPolu(F)==1)// pojedyncze bicie
          {    
               if (coJestNaPolu(F+(D-F)/2)>10 && coJestNaPolu(D)==10
                   && (D-F==8 || D-F==-8 ||D-F==10 || D-F==-10)
                   && jestZagrozony(F+(D-F)/2,1)&&max0==1) possible=true; 
          }   
            if (czym==2&&max0==0&&ileZagrozonych()==0) //sprawdza ruch damki
          {  
                      if (abs(D-F)%5==0&&D<F&&czteryCzyPiec(5,F,D)==5)
                        {   
                            possible=true;
                            for (i=1;i<((F-D)/5)+1;i++)
                                 {
                                     if (coJestNaPolu(F-i*5)!=10){possible=false;}
                                 }
                                 
                        } 
                      if (abs(D-F)%5==0&&D>F&&czteryCzyPiec(5,F,D)==5)
                        {   
                            possible=true;
                            for (i=1;i<((D-F)/5)+1;i++)
                                 {
                                     if (coJestNaPolu(F+i*5)!=10){possible=false;}
                                 }
                                 
                        } 
                      if (abs(D-F)%4==0&&D<F&&czteryCzyPiec(4,F,D)==4)
                        {   
                            possible=true;
                            for (i=1;i<((F-D)/4)+1;i++)
                                 {
                                     if (coJestNaPolu(F-i*4)!=10){possible=false;}
                                 }
                                 
                        } 
                      if (abs(D-F)%4==0&&D>F&&czteryCzyPiec(4,F,D)==4)
                        {   
                            possible=true;
                            for (i=1;i<((D-F)/4)+1;i++)
                                 {
                                     if (coJestNaPolu(F+i*4)!=10){possible=false;}
                                 }
                                 
                        }           
                      
          }  
                 if (czym==2&&ileZagrozonych()==1&&max0==1) //bicia damki czarnej    
         {                 
                  if (abs(D-F)%5==0&&D<F&&czteryCzyPiec(5,F,D)==5)
                        {   
                            possible=true;
                            i=0;
                          while (coJestNaPolu(F-(i+1)*5)==10)
                                 {
                                      i++;     
                                 }       
                                     
                                     if (coJestNaPolu(F-(i+1)*5)<4) {possible=false;}
                                     if ((F-(i+1)*5)<D) {possible=false;}
                                    
                 
                          i=i+2;          
                          while ((F-i*5)!=D)
                                 {
                                   if (coJestNaPolu(F-i*5)!=10) {possible=false;}
                                   i++;
                                 }   
                        }  
                      if (abs(D-F)%5==0&&D>F&&czteryCzyPiec(5,F,D)==5)
                        {   
                            possible=true;
                            i=0;
                            while (coJestNaPolu(F+(i+1)*5)==10)
                                 {
                                      i++;     
                                 }       
                                     
                                     if (coJestNaPolu(F+(i+1)*5)<4) {possible=false;}
                                     if ((F+(i+1)*5)>D) {possible=false;}
                                    
                         
                          i=i+2;          
                          while ((F+i*5)!=D)
                                 {
                                   if (coJestNaPolu(F+i*5)!=10) {possible=false;}
                                   i++;
                                 }                
                            
                        }    
                     if (abs(D-F)%4==0&&D<F&&czteryCzyPiec(4,F,D)==4)
                        {   
                            possible=true;
                            i=0;
                          while (coJestNaPolu(F-(i+1)*4)==10)
                                 {
                                      i++;     
                                 }       
                                     
                                     if (coJestNaPolu(F-(i+1)*4)<4||coJestNaPolu(F-(i+1)*4)==0) {possible=false;}
                                     if ((F-(i+1)*4)<D) {possible=false;}
                                    
                         
                          i=i+2;          
                          while ((F-i*4)!=D)
                                 {
                                   if (coJestNaPolu(F-i*4)!=10) {possible=false;}
                                   i++;
                                 }                                             
                        }   
                      if (abs(D-F)%4==0&&D>F&&czteryCzyPiec(4,F,D)==4)
                        {    
                            possible=true;
                            i=0;
                            while (coJestNaPolu(F+(i+1)*4)==10)
                                 {
                                      i++;     
                                 }       
                                       
                                     if (coJestNaPolu(F+(i+1)*4)<4||coJestNaPolu(F+(i+1)*4)==0) {possible=false;}
                                     if ((F+(i+1)*4)>D) {possible=false;}
                                      
                          i=i+2;          
                          while ((F+i*4)!=D)
                                 {
                                   if (coJestNaPolu(F+i*4)!=10) {possible=false;}
                                   i++;
                                 }                
                              
                        }    
                         
         }           
      }   
      if (ileZagrozonych()>1&&gdzie==10&&czym!=12&&czym!=2) 
          
          {     int max;
               ile=ileZagrozonych();
               whiteToMove?max=max1:max=max0;
                possible=true;             
                for (i=0;i<ile;i++)
                       {  
                           juz=false;
                         if (F+5<36&&myField[F+5]!=0)
                          {if (myField[F+5]->underAttack&&coJestNaPolu(F+10)==10)
                          {   
                             myField[F+5]->zbij();
                             myField[F+5]->relieve();
                             F=F+10;
                             juz=true;
                          } 
                          }       
                         
                         if (F+4<36&&myField[F+4]!=0)
                         {
                         if(myField[F+4]->underAttack&&coJestNaPolu(F+8)==10&&juz==false)
                         {   
                             myField[F+4]->zbij();
                             myField[F+4]->relieve();
                             F=F+8;
                             juz=true;
                         }
                         }    
                         
                         if (F-5<36&&myField[F-5]!=0)
                         {if(myField[F-5]->underAttack&&coJestNaPolu(F-10)==10&&juz==false)
                         { 
                             myField[F-5]->zbij();
                             myField[F-5]->relieve();
                             F=F-10;
                             juz=true;
                         }
                         }    
                         if (F-4<36&&myField[F-4]!=0)
                         {if(myField[F-4]->underAttack&&coJestNaPolu(F-8)==10&&juz==false)
                         {
                             myField[F-4]->zbij();
                             myField[F-4]->relieve();
                             F=F-8; 
                             juz=true;
                             
                         } 
                         }    
                         if (juz==false)possible=false;   
                         
                    }
                     
                if (F!=D) possible=false;    
                if (ileZagrozonych()>0) possible=false;
                if (ile!=max) possible=false;
                //if (jestBicie(0,D,1)&&!whiteToMove) possible=false;
                //if (jestBicie(1,D,1)&&whiteToMove) possible=false;
                if (possible) postawWszystkie(true);   
                if (!possible) postawWszystkie(false);
               
           }   
           

      if (ileZagrozonych()>1&&gdzie==10&&(czym==12||czym==2)) 
          
          {    int max;
               F=from;
               D=destination;
               whiteToMove?max=max1:max=max0;
               maks=max;
               ile=ileZagrozonych();
               liczbaznalezionych=0;
               possible=false;  
               skad=-1;
               kierunek=-1;
    
               postawWszystkie2();      
                  if (damkaSamoPG(F,whiteToMove)==max)
                       {   postawWszystkie2();  
                           if (sprawdzSamoPG(F,0)==max) {possible=true;}
                       }  
                     postawWszystkie2();     
                  if (damkaSamoLG(F,whiteToMove)==max)
                       {   postawWszystkie2();  
                           if (sprawdzSamoLG(F,0)==max) {possible=true;}
                       }    
                     postawWszystkie2();  
                  if (damkaSamoLD(F,whiteToMove)==max)
                       {   
                           postawWszystkie2();  
                           if (sprawdzSamoLD(F,0)==max) {possible=true;}
                       }    
                                           postawWszystkie2();  
                  if (damkaSamoPD(F,whiteToMove)==max)
                       {   
                           postawWszystkie2();  
                           if (sprawdzSamoPD(F,0)==max) {possible=true;}
                       }           
                      
                   if (skad!=-1) 
                      {
                       if (znajdzD(skad,D,kierunek)&&possible) {possible=true;}
                       else {possible=false;}
                       
                      } 
                //if (F!=D) possible=false;    
                if (ileZagrozonych()>0) possible=false;
           
                if (ile!=max) possible=false;

                //if (jestBicie(0,D,1)&&!whiteToMove) possible=false;
                //if (jestBicie(1,D,1)&&whiteToMove) possible=false;
                if (possible) postawWszystkie(true);   
                if (!possible) postawWszystkie(false);
               
           }              
  //  if (damka){ myLog<< "D="<<D<<" F="<<F<<" possible="<<possible<<endl;}               
    return possible;
   }   
//==============================================================================
void cChessboard::update(int clickedField, int hoveredField)
{    
     bool zaznaczony=false;
     bool movePossible;
     if(clickedField>=0)
       {
        if(selectedField>=0)
        {   
            if ( whiteToMove && coJestNaPolu(clickedField)<10
               &&!myField[clickedField]->underAttack)
               {myField[clickedField]->threaten(); zaznaczony=true;}  
            if ( !whiteToMove && coJestNaPolu(clickedField)>10
               &&!myField[clickedField]->underAttack)
               { myField[clickedField]->threaten(); zaznaczony=true;}       
            
            if //((!ifMovePossible(selectedField,clickedField)) && 
                ( (myField[clickedField]->occupied) &&
                (myField[clickedField]->occupiedBy->bORw==myField[selectedField]->occupiedBy->bORw))    
                 {   
                     myField[selectedField]->deselect();
                     myField[clickedField]->select();
                     selectedField = clickedField;
                     odznaczWszystkie();
                 }    
                  
                movePossible=ifMovePossible(selectedField,clickedField);
            if (movePossible&&coJestNaPolu(selectedField)!=12
                &&coJestNaPolu(selectedField)!=2)//||maxBic(whiteToMove)==0))
                 {     
                     movePawn(selectedField,clickedField);
                     myField[selectedField]->deselect();
                     selectedField=-1;
                 } 
                 
            else if (movePossible&&(coJestNaPolu(selectedField)==12
                ||coJestNaPolu(selectedField)==2))
                 {   int licznik;  
                     for (licznik=0;licznik<36;licznik++)
                      {  if (myField[licznik]!=0)
                           {
                          if (myField[licznik]->underAttack)
                              {
                                  myField[licznik]->relieve();
                                  myField[licznik]->deoccupy();
                              }
                           }          
                      }
                     myField[clickedField]->occupy(myField[selectedField]->deoccupy());
                     myField[selectedField]->deoccupy(); 
                     myField[selectedField]->deselect();
                     selectedField=-1;
                     whiteToMove=!whiteToMove;  
                     odznaczWszystkie();
                 } 
                      
            if (!myField[clickedField]->occupied && jestZagrozony())
                {
                    if (!movePossible)
                        {
                             odznaczWszystkie();
                        }    
                }    
            if (myField[clickedField]->underAttack&&!zaznaczony) {myField[clickedField]->relieve();}           
        }else    
           { 
            if (whiteToMove)
              {
               if (myField[clickedField]->occupied && myField[clickedField]->occupiedBy->bORw)
             {    
               myField[clickedField]->select();
               selectedField = clickedField;
             }
              }else
               if (myField[clickedField]->occupied && myField[clickedField]->occupiedBy->bORw==0)
             {    
               myField[clickedField]->select();
               selectedField = clickedField;
             }
           }     
      }          
} 
//==============================================================================   
bool cChessboard::jestZagrozony(int ktory, bool tylko)
{
    int i;
    bool jest=false;
    if (ktory==-1)
    {
     for (i=0; i<36;i++)
     {
         if(myField[i]!=0) 
          {
             if (myField[i]->underAttack) {jest=true;}
              
          }
     }
    }else
        {
            if (myField[ktory]->underAttack) {jest=true;}
            if (tylko==true)
                {
                 for (i=0; i<36;i++)
                       {
                          if(myField[i]!=0) 
                            {
                                if (myField[i]->underAttack&&i!=ktory) {jest=false;}
              
                            }
   
                       }    
                 }        
     }    
    return jest;
}
//==============================================================================        
void cChessboard::odznaczWszystkie(void)
{
    int i;
    for (i=0; i<36;i++) 
        { if (myField[i]!=0) 
         {
            if(myField[i]->underAttack) 
               {
                   myField[i]->relieve();
               }
         }
        }
}
//==============================================================================        
void cChessboard::postawWszystkie(bool pos)
{   
    int i;
    for (i=0; i<36;i++) 
        { if (myField[i]!=0) 
         {  
            if(myField[i]->zbity) 
               {  
                   myField[i]->postaw();
                   if (pos) 
                       {
                           myField[i]->deoccupy();
                       }     
                    if (!pos) 
                       {
                           myField[i]->threaten();
                       }   
               }
         }
        }   
}    
//==============================================================================
void cChessboard::postawWszystkie2(void)
{    
    int i;
    for (i=0; i<36;i++) 
        { if (myField[i]!=0) 
         {
            if(myField[i]->zaznaczony) 
               {
                   myField[i]->odznacz();
               }
         }
        }   
}    
//==============================================================================
int cChessboard::ileZagrozonych(void)
{
    int i;
    int ile=0;
    for (i=0; i<36;i++) 
        { if (myField[i]!=0) 
         {
            if(myField[i]->underAttack) ile++;
         }
        }  
    return ile;      
}    
//==============================================================================
void cChessboard::movePawn(int from, int destination)
{
        int F = myField[from]->xBoard;
        int D = myField[destination]->xBoard;
        int kolor;
        if (whiteToMove) kolor=1;
        if (!whiteToMove) kolor=0;
            if (abs(destination-from)>7&&ileZagrozonych()==1&&coJestNaPolu(from)!=12&&coJestNaPolu(from)!=2)
                {
                    myField[from+((destination-from)/2)]->deoccupy();
                    myField[from+((destination-from)/2)]->relieve();  
                }
              
        myField[destination]->occupy(myField[from]->deoccupy());
            switch(myField[destination]->occupiedBy->bORw){
                case 1 :
                    if(int(myField[destination]->xBoard) > 31) 
                        myField[destination]->crownPawn();
                    break;
                case 0 :
                    if(int(myField[destination]->xBoard) < 4) 
                        myField[destination]->crownPawn();
                    break;
                default :
                    break;
            }   
           whiteToMove=!whiteToMove;  
           odznaczWszystkie();
       
        
}
//==============================================================================
int cChessboard::ileBic(int ktory)
{   
    int max=0; 
    if (coJestNaPolu(ktory)==11)
    {
                            if (ktory<26&&coJestNaPolu(ktory+5)>0)
                            {
                              if (coJestNaPolu(ktory+5)<10&&(!myField[ktory+5]->zaznaczony)
                              && coJestNaPolu(ktory+10)==10)
                                {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezLD(ktory+10,1);
                                   max=liczbabic;
                                   liczbabic=0;
                                } 
                            }
                            if (ktory<28&&coJestNaPolu(ktory+4)>0)
                            {    
                             if (coJestNaPolu(ktory+4)<10&&coJestNaPolu(ktory+8)==10
                             &&(!myField[ktory+4]->zaznaczony))
                               {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezPD(ktory+8,1);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                               }
                            }
                            if (ktory>9&&coJestNaPolu(ktory-5)>0)
                            {    
                             if (coJestNaPolu(ktory-5)<10 && coJestNaPolu(ktory-10)==10
                             &&(!myField[ktory-5]->zaznaczony))
                               {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezPG(ktory-10,1);
                                   if (liczbabic>max) max=liczbabic;
                                   liczbabic=0;
                               }
                            } 
                            if(ktory>7&&coJestNaPolu(ktory-4)>0)
                            {   
                             if (coJestNaPolu(ktory-4)<10 && coJestNaPolu(ktory-8)==10
                             &&(!myField[ktory-4]->zaznaczony))
                              {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezLG(ktory-8,1);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                              }
                            }              
    }
  if (coJestNaPolu(ktory)==1)
    {
                            if (ktory<26&&coJestNaPolu(ktory+5)>0)
                            {
                              if (coJestNaPolu(ktory+5)>10&&(!myField[ktory+5]->zaznaczony)
                              && coJestNaPolu(ktory+10)==10)
                                {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezLD(ktory+10,0);
                                   max=liczbabic;
                                   liczbabic=0;
                                } 
                            }
                            if (ktory<28&&coJestNaPolu(ktory+4)>0)
                            {    
                             if (coJestNaPolu(ktory+4)>10&&coJestNaPolu(ktory+8)==10
                             &&(!myField[ktory+4]->zaznaczony))
                               {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezPD(ktory+8,0);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                               }
                            }
                            if (ktory>9&&coJestNaPolu(ktory-5)>0)
                            {    
                             if (coJestNaPolu(ktory-5)>10 && coJestNaPolu(ktory-10)==10
                             &&(!myField[ktory-5]->zaznaczony))
                               {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezPG(ktory-10,0);
                                   if (liczbabic>max) max=liczbabic;
                                   liczbabic=0;
                               }
                            } 
                            if(ktory>7&&coJestNaPolu(ktory-4)>0)
                            {   
                             if (coJestNaPolu(ktory-4)>10 && coJestNaPolu(ktory-8)==10
                             &&(!myField[ktory-4]->zaznaczony))
                              {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezLG(ktory-8,0);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                              }
                            }              
    }  
   if  (coJestNaPolu(ktory)==12)
       {     postawWszystkie2();
              
             liczbabic=0;
             liczbabic=damkaSamoPG(ktory,1);
              if(liczbabic>max) max=liczbabic;
              liczbabic=0;
              postawWszystkie2();
             liczbabic=damkaSamoLG(ktory,1);
              if(liczbabic>max) max=liczbabic;
              liczbabic=0;
              postawWszystkie2();
             liczbabic=damkaSamoLD(ktory,1);
              if(liczbabic>max) max=liczbabic;
              liczbabic=0;
              postawWszystkie2();
             liczbabic=damkaSamoPD(ktory,1);
                  if(liczbabic>max) max=liczbabic;
                  liczbabic=0;
                   
                  
       }
 if  (coJestNaPolu(ktory)==2)
       {     postawWszystkie2();
              
             liczbabic=0;
             liczbabic=damkaSamoPG(ktory,0);
              if(liczbabic>max) max=liczbabic;
              liczbabic=0;
              postawWszystkie2();
             liczbabic=damkaSamoLG(ktory,0);
              if(liczbabic>max) max=liczbabic;
              liczbabic=0;
              postawWszystkie2();
             liczbabic=damkaSamoLD(ktory,0);
              if(liczbabic>max) max=liczbabic;
              liczbabic=0;
              postawWszystkie2();
             liczbabic=damkaSamoPD(ktory,0);
                  if(liczbabic>max) max=liczbabic;
                  liczbabic=0;
                  liczbabic=0;
       }      
  
 return max;       
}  
//==============================================================================
int cChessboard::bezLG(int ktory, int kolor)
{   int max=0;
    if (kolor==1)
    {
                            if (ktory<26&&coJestNaPolu(ktory+5)>0)
                            {
                              if (coJestNaPolu(ktory+5)<10&&(!myField[ktory+5]->zaznaczony)
                              && coJestNaPolu(ktory+10)==10)
                                {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezLD(ktory+10,1);
                                   max=liczbabic;
                                   liczbabic=0;
                                } 
                            }
                            if (ktory>9&&coJestNaPolu(ktory-5)>0)
                            {    
                             if (coJestNaPolu(ktory-5)<10 && coJestNaPolu(ktory-10)==10
                             &&(!myField[ktory-5]->zaznaczony))
                               {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezPG(ktory-10,1);
                                   if (liczbabic>max) max=liczbabic;
                                   liczbabic=0;
                               }
                            } 
                            if(ktory>7&&coJestNaPolu(ktory-4)>0)
                            {   
                             if (coJestNaPolu(ktory-4)<10 && coJestNaPolu(ktory-8)==10
                             &&(!myField[ktory-4]->zaznaczony))
                              {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezLG(ktory-8,1);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                              }
                            }              
    } 
    if (kolor==0)
    {
                            if (ktory<26&&coJestNaPolu(ktory+5)>0)
                            {
                              if (coJestNaPolu(ktory+5)>10&&(!myField[ktory+5]->zaznaczony)
                              && coJestNaPolu(ktory+10)==10)
                                {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezLD(ktory+10,0);
                                   max=liczbabic;
                                   liczbabic=0;
                                } 
                            }
                            if (ktory>9&&coJestNaPolu(ktory-5)>0)
                            {    
                             if (coJestNaPolu(ktory-5)>10 && coJestNaPolu(ktory-10)==10
                             &&(!myField[ktory-5]->zaznaczony))
                               {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezPG(ktory-10,0);
                                   if (liczbabic>max) max=liczbabic;
                                   liczbabic=0;
                               }
                            } 
                            if(ktory>7&&coJestNaPolu(ktory-4)>0)
                            {   
                             if (coJestNaPolu(ktory-4)>10 && coJestNaPolu(ktory-8)==10
                             &&(!myField[ktory-4]->zaznaczony))
                              {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezLG(ktory-8,0);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                              }
                            }              
    } 
    return max; 
}    
//==============================================================================
int cChessboard::bezLD(int ktory, int kolor)
{    int max=0;
     if (kolor==1)
    {
                            if (ktory<26&&coJestNaPolu(ktory+5)>0)
                            {
                              if (coJestNaPolu(ktory+5)<10&&(!myField[ktory+5]->zaznaczony)
                              && coJestNaPolu(ktory+10)==10)
                                {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezLD(ktory+10,1);
                                   max=liczbabic;
                                   liczbabic=0;
                                } 
                            }
                            if (ktory<28&&coJestNaPolu(ktory+4)>0)
                            {    
                             if (coJestNaPolu(ktory+4)<10&&coJestNaPolu(ktory+8)==10
                             &&(!myField[ktory+4]->zaznaczony))
                               {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezPD(ktory+8,1);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                               }
                            }
                            if(ktory>7&&coJestNaPolu(ktory-4)>0)
                            {   
                             if (coJestNaPolu(ktory-4)<10 && coJestNaPolu(ktory-8)==10
                             &&(!myField[ktory-4]->zaznaczony))
                              {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezLG(ktory-8,1);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                              }
                            }              
    }
    if (kolor==0)
    {
                            if (ktory<26&&coJestNaPolu(ktory+5)>0)
                            {
                              if (coJestNaPolu(ktory+5)>10&&(!myField[ktory+5]->zaznaczony)
                              && coJestNaPolu(ktory+10)==10)
                                {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezLD(ktory+10,0);
                                   max=liczbabic;
                                   liczbabic=0;
                                } 
                            }
                            if (ktory<28&&coJestNaPolu(ktory+4)>0)
                            {    
                             if (coJestNaPolu(ktory+4)>10&&coJestNaPolu(ktory+8)==10
                             &&(!myField[ktory+4]->zaznaczony))
                               {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezPD(ktory+8,0);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                               }
                            }
                            if(ktory>7&&coJestNaPolu(ktory-4)>0)
                            {   
                             if (coJestNaPolu(ktory-4)>10 && coJestNaPolu(ktory-8)==10
                             &&(!myField[ktory-4]->zaznaczony))
                              {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezLG(ktory-8,0);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                              }
                            }              
    }
   return max; 
}    
//==============================================================================
int cChessboard::bezPG(int ktory, int kolor)
{
      int max=0;
     if (kolor==1)
    {
                            if (ktory<28&&coJestNaPolu(ktory+4)>0)
                            {    
                             if (coJestNaPolu(ktory+4)<10&&coJestNaPolu(ktory+8)==10
                             &&(!myField[ktory+4]->zaznaczony))
                               {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezPD(ktory+8,1);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                               }
                            }
                            if (ktory>9&&coJestNaPolu(ktory-5)>0)
                            {    
                             if (coJestNaPolu(ktory-5)<10 && coJestNaPolu(ktory-10)==10
                             &&(!myField[ktory-5]->zaznaczony))
                               {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezPG(ktory-10,1);
                                   if (liczbabic>max) max=liczbabic;
                                   liczbabic=0;
                               }
                            } 
                            if(ktory>7&&coJestNaPolu(ktory-4)>0)
                            {   
                             if (coJestNaPolu(ktory-4)<10 && coJestNaPolu(ktory-8)==10
                             &&(!myField[ktory-4]->zaznaczony))
                              {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezLG(ktory-8,1);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                              }
                            }              
    }
      if (kolor==0)
    {
                            if (ktory<28&&coJestNaPolu(ktory+4)>0)
                            {    
                             if (coJestNaPolu(ktory+4)>10&&coJestNaPolu(ktory+8)==10
                             &&(!myField[ktory+4]->zaznaczony))
                               {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezPD(ktory+8,0);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                               }
                            }
                            if (ktory>9&&coJestNaPolu(ktory-5)>0)
                            {    
                             if (coJestNaPolu(ktory-5)>10 && coJestNaPolu(ktory-10)==10
                             &&(!myField[ktory-5]->zaznaczony))
                               {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezPG(ktory-10,0);
                                   if (liczbabic>max) max=liczbabic;
                                   liczbabic=0;
                               }
                            } 
                            if(ktory>7&&coJestNaPolu(ktory-4)>0)
                            {   
                             if (coJestNaPolu(ktory-4)>10 && coJestNaPolu(ktory-8)==10
                             &&(!myField[ktory-4]->zaznaczony))
                              {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezLG(ktory-8,0);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                              }
                            }              
    }
   return max;
}    
//==============================================================================
int cChessboard::bezPD(int ktory, int kolor)     
{
      int max=0;
     if (kolor==1)
    {
                            if (ktory<26&&coJestNaPolu(ktory+5)>0)
                            {
                              if (coJestNaPolu(ktory+5)<10&&(!myField[ktory+5]->zaznaczony)
                              && coJestNaPolu(ktory+10)==10)
                                {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezLD(ktory+10,1);
                                   max=liczbabic;
                                   liczbabic=0;
                                } 
                            }
                            if (ktory<28&&coJestNaPolu(ktory+4)>0)
                            {    
                             if (coJestNaPolu(ktory+4)<10&&coJestNaPolu(ktory+8)==10
                             &&(!myField[ktory+4]->zaznaczony))
                               {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezPD(ktory+8,1);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                               }
                            }
                            if (ktory>9&&coJestNaPolu(ktory-5)>0)
                            {    
                             if (coJestNaPolu(ktory-5)<10 && coJestNaPolu(ktory-10)==10
                             &&(!myField[ktory-5]->zaznaczony))
                               {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezPG(ktory-10,1);
                                   if (liczbabic>max) max=liczbabic;
                                   liczbabic=0;
                               }
                            } 
    }
   if (kolor==0)
    {
                            if (ktory<26&&coJestNaPolu(ktory+5)>0)
                            {
                              if (coJestNaPolu(ktory+5)>10&&(!myField[ktory+5]->zaznaczony)
                              && coJestNaPolu(ktory+10)==10)
                                {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezLD(ktory+10,0);
                                   max=liczbabic;
                                   liczbabic=0;
                                } 
                            }
                            if (ktory<28&&coJestNaPolu(ktory+4)>0)
                            {    
                             if (coJestNaPolu(ktory+4)>10&&coJestNaPolu(ktory+8)==10
                             &&(!myField[ktory+4]->zaznaczony))
                               {
                                 liczbabic++;
                                 myField[ktory]->zaznacz();
                                 liczbabic+=bezPD(ktory+8,0);
                                 if (liczbabic>max) max=liczbabic;
                                 liczbabic=0;
                               }
                            }
                            if (ktory>9&&coJestNaPolu(ktory-5)>0)
                            {    
                             if (coJestNaPolu(ktory-5)>10 && coJestNaPolu(ktory-10)==10
                             &&(!myField[ktory-5]->zaznaczony))
                               {
                                   liczbabic++;
                                   myField[ktory]->zaznacz();
                                   liczbabic+=bezPG(ktory-10,0);
                                   if (liczbabic>max) max=liczbabic;
                                   liczbabic=0;
                               }
                            } 
    } 
   return max;
}
//==============================================================================
int cChessboard::damkaLG(int ktory, int kolor)
{    int max=0;
    int taliczbabic2=0; int taliczbabic=0;
if  (kolor==1)
          {    
               myField[ktory]->zaznacz();
             if (ktory+4<36)  
              {if (coJestNaPolu(ktory+4)==10&&!myField[ktory+4]->zaznaczony)
                 {   
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory+4,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLG(ktory+4,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory+4,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              }  }
             if (ktory+8<36) 
              {if (coJestNaPolu(ktory+4)<10&&coJestNaPolu(ktory+4)>0
                 && coJestNaPolu(ktory+8)==10&&!myField[ktory+8]->zaznaczony
                 &&!myField[ktory+4]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory+4]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory+8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLG(ktory+8,1); 
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory+8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
         } 
if  (kolor==0)
         {   
               myField[ktory]->zaznacz();
             if (ktory+4<36)  
              {if (coJestNaPolu(ktory+4)==10&&!myField[ktory+4]->zaznaczony)
                 {   
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory+4,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLG(ktory+4,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory+4,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              }  }
             if (ktory+8<36) 
              {if (coJestNaPolu(ktory+4)>10
                 && coJestNaPolu(ktory+8)==10&&!myField[ktory+8]->zaznaczony
                 &&!myField[ktory+4]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory+4]->zaznacz();
             
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory+8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLG(ktory+8,0); 
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory+8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
      
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
         }
     return max;
}    
//==============================================================================
int cChessboard::damkaLD(int ktory, int kolor)
{    int max=0;
      int taliczbabic2=0; int taliczbabic=0;
if  (kolor==1)
       {   
           myField[ktory]->zaznacz();
             if (ktory-5>-1)  
              {if (coJestNaPolu(ktory-5)==10&&!myField[ktory-5]->zaznaczony)
                 {   
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory-5,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLD(ktory-5,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory-5,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              }  }
             if (ktory-10>-1) 
              {if (coJestNaPolu(ktory-5)<10&&coJestNaPolu(ktory-5)>0
                 && coJestNaPolu(ktory-10)==10&&!myField[ktory-10]->zaznaczony
                 &&!myField[ktory-5]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory-5]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory-10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLD(ktory-10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory-10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
if  (kolor==0)
 {   
           myField[ktory]->zaznacz();
             if (ktory-5>-1)  
              {if (coJestNaPolu(ktory-5)==10&&!myField[ktory-5]->zaznaczony)
                 {   
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory-5,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLD(ktory-5,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory-5,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              }  }
             if (ktory-10>-1) 
              {if (coJestNaPolu(ktory-5)>10
                 && coJestNaPolu(ktory-10)==10&&!myField[ktory-10]->zaznaczony
                 &&!myField[ktory-5]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory-5]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory-10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLD(ktory-10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory-10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }     
     return max;
}    
//==============================================================================
int cChessboard::damkaPG(int ktory, int kolor)
{    int max=0;
     int taliczbabic2=0; int taliczbabic=0;                          
           
if  (kolor==1)
       {   
            myField[ktory]->zaznacz();
             if (ktory+5<36)  
              {if (coJestNaPolu(ktory+5)==10&&!myField[ktory+5]->zaznaczony)
                 {   
                     taliczbabic2=taliczbabic2+damkaPG(ktory+5,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory+5,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory+5,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              }  }
             if (ktory+10<36) 
              {if (coJestNaPolu(ktory+5)<10&&coJestNaPolu(ktory+5)>0
                 && coJestNaPolu(ktory+10)==10&&!myField[ktory+10]->zaznaczony
                 &&!myField[ktory+5]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory+5]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaPG(ktory+10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory+10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory+10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
          } 
if  (kolor==0)
       {   
            myField[ktory]->zaznacz();
             if (ktory+5<36)  
              {if (coJestNaPolu(ktory+5)==10&&!myField[ktory+5]->zaznaczony)
                 {   
                     taliczbabic2=taliczbabic2+damkaPG(ktory+5,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory+5,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory+5,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              }  }
             if (ktory+10<36) 
              {if (coJestNaPolu(ktory+5)>10
                 && coJestNaPolu(ktory+10)==10&&!myField[ktory+10]->zaznaczony
                 &&!myField[ktory+5]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory+5]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaPG(ktory+10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory+10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory+10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
          }       
     return max;
}    
//==============================================================================
int cChessboard::damkaPD(int ktory, int kolor)
{    int max=0;
     int taliczbabic2=0; int taliczbabic=0;
if  (kolor==1)
       {    
           myField[ktory]->zaznacz();
             if (ktory-4>-1)  
              {if (coJestNaPolu(ktory-4)==10&&!myField[ktory-4]->zaznaczony)
                 {   
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory-4,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory-4,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaPD(ktory-4,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              }  }
             if (ktory-8>-1) 
              {if (coJestNaPolu(ktory-4)<10&&coJestNaPolu(ktory-4)>0
                 && coJestNaPolu(ktory-8)==10&&!myField[ktory-8]->zaznaczony
                 &&!myField[ktory-4]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory-4]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory-8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory-8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaPD(ktory-8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
if  (kolor==0)
     {    
           myField[ktory]->zaznacz();
             if (ktory-4>-1)  
              {if (coJestNaPolu(ktory-4)==10&&!myField[ktory-4]->zaznaczony)
                 {   
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory-4,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory-4,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaPD(ktory-4,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              }  }
             if (ktory-8>-1) 
              {if (coJestNaPolu(ktory-4)>10
                 && coJestNaPolu(ktory-8)==10&&!myField[ktory-8]->zaznaczony
                 &&!myField[ktory-4]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory-4]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory-8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory-8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaPD(ktory-8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
     return max;  
}
//==============================================================================
int cChessboard::damkaSamoPG(int ktory, int kolor)
{    int max=0;
     int taliczbabic2=0; int taliczbabic=0;                          
           
if  (kolor==1)
       {     
           myField[ktory]->zaznacz();
             if (ktory+5<36)  
              {if (coJestNaPolu(ktory+5)==10&&!myField[ktory+5]->zaznaczony)
                 {   
                     taliczbabic=taliczbabic+damkaSamoPG(ktory+5,1);
              }  }
             if (ktory+10<36) 
              {if (coJestNaPolu(ktory+5)<10&&coJestNaPolu(ktory+5)>0
                 && coJestNaPolu(ktory+10)==10&&!myField[ktory+10]->zaznaczony
                 &&!myField[ktory+5]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory+5]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaPG(ktory+10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory+10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory+10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
          }      
if  (kolor==0)
       {     
           myField[ktory]->zaznacz();
             if (ktory+5<36)  
              {if (coJestNaPolu(ktory+5)==10&&!myField[ktory+5]->zaznaczony)
                 {   
                     taliczbabic=taliczbabic+damkaSamoPG(ktory+5,0);
              }  }
             if (ktory+10<36) 
              {if (coJestNaPolu(ktory+5)>10
                 && coJestNaPolu(ktory+10)==10&&!myField[ktory+10]->zaznaczony
                 &&!myField[ktory+5]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory+5]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaPG(ktory+10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory+10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory+10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
          }      
     return max;
}    
//==============================================================================
int cChessboard::damkaSamoLG(int ktory, int kolor)
{
   int max=0;
   int taliczbabic2=0; int taliczbabic=0;
if  (kolor==1)
       {       
               myField[ktory]->zaznacz();
             if (ktory+4<36)  
              {if (coJestNaPolu(ktory+4)==10&&!myField[ktory+4]->zaznaczony)
                 {   
                     taliczbabic=taliczbabic+damkaSamoLG(ktory+4,1);
              }  }
             if (ktory+8<36) 
              {if (coJestNaPolu(ktory+4)<10&&coJestNaPolu(ktory+4)>0
                 && coJestNaPolu(ktory+8)==10&&!myField[ktory+8]->zaznaczony
                 &&!myField[ktory+4]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory+4]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory+8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;cout <<"Tego niema"<<endl;}
                     taliczbabic2=taliczbabic2+damkaLG(ktory+8,1); 
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;cout <<"Tego niema"<<endl;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory+8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;cout <<"Tego niema"<<endl;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
if  (kolor==0)
       {       
               myField[ktory]->zaznacz();
             if (ktory+4<36)  
              {if (coJestNaPolu(ktory+4)==10&&!myField[ktory+4]->zaznaczony)
                 {   
                     taliczbabic=taliczbabic+damkaSamoLG(ktory+4,0);
              }  }
             if (ktory+8<36) 
              {if (coJestNaPolu(ktory+4)>10
                 && coJestNaPolu(ktory+8)==10&&!myField[ktory+8]->zaznaczony
                 &&!myField[ktory+4]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory+4]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory+8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;cout <<"Tego niema"<<endl;}
                     taliczbabic2=taliczbabic2+damkaLG(ktory+8,0); 
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;cout <<"Tego niema"<<endl;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory+8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;cout <<"Tego niema"<<endl;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
     return max;
        
}    
//==============================================================================
int cChessboard::damkaSamoLD(int ktory, int kolor)
{   
     int max=0;
     int taliczbabic2=0; int taliczbabic=0;
if  (kolor==1)
       {   
           myField[ktory]->zaznacz();
             if (ktory-5>-1)  
              {if (coJestNaPolu(ktory-5)==10&&!myField[ktory-5]->zaznaczony)
                 {   
                     taliczbabic=taliczbabic+damkaSamoLD(ktory-5,1);
              }  }
             if (ktory-10>-1) 
              {if (coJestNaPolu(ktory-5)<10&&coJestNaPolu(ktory-5)>0
                 && coJestNaPolu(ktory-10)==10&&!myField[ktory-10]->zaznaczony
                 &&!myField[ktory-5]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory-5]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory-10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLD(ktory-10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory-10,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
if  (kolor==0)
       {   
           myField[ktory]->zaznacz();
             if (ktory-5>-1)  
              {if (coJestNaPolu(ktory-5)==10&&!myField[ktory-5]->zaznaczony)
                 {   
                     taliczbabic=taliczbabic+damkaSamoLD(ktory-5,0);
              }  }
             if (ktory-10>-1) 
              {if (coJestNaPolu(ktory-5)>10
                 && coJestNaPolu(ktory-10)==10&&!myField[ktory-10]->zaznaczony
                 &&!myField[ktory-5]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory-5]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoLG(ktory-10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaLD(ktory-10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoPD(ktory-10,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
     return max;    
}    
//==============================================================================
int cChessboard::damkaSamoPD(int ktory, int kolor)
{    int max=0;
     int taliczbabic2=0; int taliczbabic=0;
if  (kolor==1)
       { 
           myField[ktory]->zaznacz();
             if (ktory-4>-1)  
              {if (coJestNaPolu(ktory-4)==10&&!myField[ktory-4]->zaznaczony)
                 {   
                     taliczbabic=taliczbabic+damkaSamoPD(ktory-4,1);
              }  }
             if (ktory-8>-1) 
              {if (coJestNaPolu(ktory-4)<10&&coJestNaPolu(ktory-4)>0
                 && coJestNaPolu(ktory-8)==10&&!myField[ktory-8]->zaznaczony
                 &&!myField[ktory-4]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory-4]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory-8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory-8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaPD(ktory-8,1);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
if  (kolor==0)
       { 
           myField[ktory]->zaznacz();
             if (ktory-4>-1)  
              {if (coJestNaPolu(ktory-4)==10&&!myField[ktory-4]->zaznaczony)
                 {   
                     taliczbabic=taliczbabic+damkaSamoPD(ktory-4,0);
              }  }
             if (ktory-8>-1) 
              {if (coJestNaPolu(ktory-4)>10
                 && coJestNaPolu(ktory-8)==10&&!myField[ktory-8]->zaznaczony
                 &&!myField[ktory-4]->zaznaczony)
                 {   
                     taliczbabic++;
                     myField[ktory-4]->zaznacz();
                     taliczbabic2=taliczbabic2+damkaSamoPG(ktory-8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaSamoLD(ktory-8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
                     taliczbabic2=taliczbabic2+damkaPD(ktory-8,0);
                     if (taliczbabic2>taliczbabic) {taliczbabic=taliczbabic2;taliczbabic2=0;} if (taliczbabic2==taliczbabic) {taliczbabic+=taliczbabic2;taliczbabic2=0;}
              } }           
              
                  if(taliczbabic>max) max=taliczbabic;
                  taliczbabic=0;
                    
       }  
     return max;
}
//==============================================================================    
int cChessboard::sprawdzLG(int ktory, int ile)
{            int max=0;
             myField[ktory]->zaznacz();
             if (ktory+4<36&&myField[ktory+4]!=0)  
              {if (coJestNaPolu(ktory+4)==10&&!myField[ktory+4]->zaznaczony)
                 {   
                     max=max+sprawdzSamoPG(ktory+4,ile);
                     max=max+sprawdzLG(ktory+4,ile);
                     max=max+sprawdzSamoLD(ktory+4,ile);
              }  }
             if (ktory+8<36&&myField[ktory+8]!=0&&myField[ktory+4]!=0) 
              {if (coJestNaPolu(ktory+8)==10&&!myField[ktory+8]->zaznaczony
                 &&!myField[ktory+4]->zaznaczony&&myField[ktory+4]->underAttack)
                 {   
                     max++; liczbaznalezionych++;
                     if (liczbaznalezionych==maks)
                         {
                             skad=ktory+8;
                             kierunek=1;
                         }    
                     myField[ktory+4]->zaznacz();
                     myField[ktory+4]->relieve();
                     myField[ktory+4]->zbij();
                     max=max+sprawdzSamoPG(ktory+8,ile+1);
                     max=max+sprawdzLG(ktory+8,ile+1); 
                     max=max+sprawdzSamoLD(ktory+8,ile+1);
              } }           
              
    return max;     
}         
//==============================================================================
int cChessboard::sprawdzLD(int ktory, int ile)
{            int max=0;
             myField[ktory]->zaznacz();
             if (ktory-5>-1&&myField[ktory-5]!=0)  
              {if (coJestNaPolu(ktory-5)==10&&!myField[ktory-5]->zaznaczony)
                 {   
                     max=max+sprawdzSamoLG(ktory-5,ile);
                     max=max+sprawdzLD(ktory-5,ile);
                     max=max+sprawdzSamoPD(ktory-5,ile);
              }  }
             if (ktory-10>-1&&myField[ktory-10]!=0&&myField[ktory-5]!=0) 
              {if (coJestNaPolu(ktory-10)==10&&!myField[ktory-10]->zaznaczony
                 &&!myField[ktory-5]->zaznaczony&&myField[ktory-5]->underAttack)
                 {   
                     max++; liczbaznalezionych++;
                     if (liczbaznalezionych==maks)
                         {
                             skad=ktory-10;
                             kierunek=2;
                         }
                     myField[ktory-5]->zaznacz();
                     myField[ktory-5]->relieve();
                     myField[ktory-5]->zbij();
                     max=max+sprawdzSamoLG(ktory-10,ile+1);
                     max=max+sprawdzLD(ktory-10,ile+1); 
                     max=max+sprawdzSamoPD(ktory-10,ile+1);
              } }           
              
    return max;     
}         
//==============================================================================
int cChessboard::sprawdzPG(int ktory, int ile)
{            int max=0;
             myField[ktory]->zaznacz();
             if (ktory+5<36&&myField[ktory+5]!=0)  
              {if (coJestNaPolu(ktory+5)==10&&!myField[ktory+5]->zaznaczony)
                 {   
                     max=max+sprawdzPG(ktory+5,ile);
                     max=max+sprawdzSamoLG(ktory+5,ile);
                     max=max+sprawdzSamoPD(ktory+5,ile);
              }  }
             if (ktory+10<36&&myField[ktory+10]!=0&&myField[ktory+5]!=0) 
              {if (coJestNaPolu(ktory+10)==10&&!myField[ktory+5]->zaznaczony
                 &&!myField[ktory+5]->zaznaczony&&myField[ktory+5]->underAttack)
                 {   
                     max++; liczbaznalezionych++;
                     if (liczbaznalezionych==maks)
                         {
                             skad=ktory+10;
                             kierunek=0;
                         }
                     myField[ktory+5]->zaznacz();
                     myField[ktory+5]->relieve();
                     myField[ktory+5]->zbij();
                     max=max+sprawdzPG(ktory+5,ile+1);
                     max=max+sprawdzSamoLG(ktory+5,ile+1); 
                     max=max+sprawdzSamoPD(ktory+5,ile+1);
              } }           
              
    return max;
}         
//==============================================================================
int cChessboard::sprawdzPD(int ktory, int ile)
{            int max=0;
             myField[ktory]->zaznacz();
             if (ktory-4>-1&&myField[ktory-4]!=0)  
              {if (coJestNaPolu(ktory-4)==10&&!myField[ktory-4]->zaznaczony)
                 {   
                     max=max+sprawdzSamoPG(ktory-4,ile);
                     max=max+sprawdzSamoLD(ktory-4,ile);
                     max=max+sprawdzPD(ktory-4,ile);
              }  }
             if (ktory-8>-1&&myField[ktory-8]!=0&&myField[ktory-4]!=0) 
              {if (coJestNaPolu(ktory-8)==10&&!myField[ktory-4]->zaznaczony
                 &&!myField[ktory-4]->zaznaczony&&myField[ktory-4]->underAttack)
                 {   
                     max++; liczbaznalezionych++;
                     if (liczbaznalezionych==maks)
                         {
                             skad=ktory-8;
                             kierunek=3;
                         }
                     myField[ktory-4]->zaznacz();
                     myField[ktory-4]->relieve();
                     myField[ktory-4]->zbij();
                     max=max+sprawdzSamoPG(ktory-4,ile+1);
                     max=max+sprawdzSamoLD(ktory-4,ile+1); 
                     max=max+sprawdzPD(ktory-4,ile+1);
              } }           
              
    return max; 
}        
//==============================================================================
int cChessboard::sprawdzSamoLG(int ktory, int ile)
{            int max=0;
             myField[ktory]->zaznacz();
             if (ktory+4<36&&myField[ktory+4]!=0)  
              {if (coJestNaPolu(ktory+4)==10&&!myField[ktory+4]->zaznaczony)
                 {   
                     max=max+sprawdzSamoLG(ktory+4,ile);
              }  }
             if (ktory+8<36&&myField[ktory+8]!=0&&myField[ktory+4]!=0) 
              {if (coJestNaPolu(ktory+8)==10&&!myField[ktory+8]->zaznaczony
                 &&!myField[ktory+4]->zaznaczony&&myField[ktory+4]->underAttack)
                 {   
                     max++; liczbaznalezionych++;
                     if (liczbaznalezionych==maks)
                         {
                             skad=ktory+8;
                             kierunek=1;
                         }
                     myField[ktory+4]->zaznacz();
                     myField[ktory+4]->relieve();
                     myField[ktory+4]->zbij();
                     max=max+sprawdzSamoPG(ktory+8,ile+1);
                     max=max+sprawdzLG(ktory+8,ile+1); 
                     max=max+sprawdzSamoLD(ktory+8,ile+1);
              } }           
              
    return max;   
}      
//==============================================================================
int cChessboard::sprawdzSamoLD(int ktory, int ile)
{            int max=0;
             myField[ktory]->zaznacz();
             if (ktory-5>-1&&myField[ktory-5]!=0)  
              {if (coJestNaPolu(ktory-5)==10&&!myField[ktory-5]->zaznaczony)
                 {   
                     max=max+sprawdzSamoLD(ktory-5,ile);
              }  }
             if (ktory-10>-1&&myField[ktory-10]!=0&&myField[ktory-5]!=0) 
              {if (coJestNaPolu(ktory-10)==10&&!myField[ktory-10]->zaznaczony
                 &&!myField[ktory-5]->zaznaczony&&myField[ktory-5]->underAttack)
                 {   
                     max++; liczbaznalezionych++;
                     if (liczbaznalezionych==maks)
                         {
                             skad=ktory-10;
                             kierunek=2;
                         }
                     myField[ktory-5]->zaznacz();
                     myField[ktory-5]->relieve();
                     myField[ktory-5]->zbij();
                     max=max+sprawdzSamoLG(ktory-10,ile+1);
                     max=max+sprawdzLD(ktory-10,ile+1); 
                     max=max+sprawdzSamoPD(ktory-10,ile+1);
              } }           
              
    return max;
}         
//==============================================================================
int cChessboard::sprawdzSamoPG(int ktory, int ile)
{            int max=0;
             myField[ktory]->zaznacz();
             if (ktory+5<36&&myField[ktory+5]!=0)  
              {if (coJestNaPolu(ktory+5)==10&&!myField[ktory+5]->zaznaczony)
                 {   
                     max=max+sprawdzSamoPG(ktory+5,ile);
              }  }
             if (ktory+10<36&&myField[ktory+10]!=0&&myField[ktory+5]!=0) 
              {if (coJestNaPolu(ktory+10)==10&&!myField[ktory+5]->zaznaczony
                 &&!myField[ktory+5]->zaznaczony&&myField[ktory+5]->underAttack)
                 {   
                     max++; liczbaznalezionych++;
                     if (liczbaznalezionych==maks)
                         {
                             skad=ktory+10;
                             kierunek=0;
                         }
                     myField[ktory+5]->zaznacz();
                     myField[ktory+5]->relieve();
                     myField[ktory+5]->zbij();
                     max=max+sprawdzPG(ktory+5,ile+1);
                     max=max+sprawdzSamoLG(ktory+5,ile+1); 
                     max=max+sprawdzSamoPD(ktory+5,ile+1);
              } }           
              
    return max;
}         
//==============================================================================
int cChessboard::sprawdzSamoPD(int ktory, int ile)
{            int max=0;
             myField[ktory]->zaznacz();
             if (ktory-4>-1&&myField[ktory-4]!=0)  
              {if (coJestNaPolu(ktory-4)==10&&!myField[ktory-4]->zaznaczony)
                 {   
                     max=max+sprawdzSamoPD(ktory-4,ile);
              }  }
             if (ktory-8>-1&&myField[ktory-8]!=0&&myField[ktory-4]!=0) 
              {if (coJestNaPolu(ktory-8)==10&&!myField[ktory-4]->zaznaczony
                 &&!myField[ktory-4]->zaznaczony&&myField[ktory-4]->underAttack)
                 {   
                     max++; liczbaznalezionych++;
                     if (liczbaznalezionych==maks)
                         {
                             skad=ktory-8;
                             kierunek=3;
                         }
                     myField[ktory-4]->zaznacz();
                     myField[ktory-4]->relieve();
                     myField[ktory-4]->zbij();
                     max=max+sprawdzSamoPG(ktory-4,ile+1);
                     max=max+sprawdzSamoLD(ktory-4,ile+1); 
                     max=max+sprawdzPD(ktory-4,ile+1);
              } }           
              
    return max; 
}        
//==============================================================================
bool cChessboard::znajdzD(int skad, int D,int kierunek)  //0-PG 1-LG 2-LD 3-PD
{   
    int i=1;
    bool znalazl=false;
    if (D==skad) {znalazl=true;}
    if (kierunek==0)
    {while (skad+(i*5)<36&&myField[skad+(i*5)]!=0&&coJestNaPolu(skad+(i*5))==10)
           {     
               if (skad+(i*5)==D) {znalazl=true;}
               i++;
    }      }  
    i=1;  
    if (kierunek==2)     
    {while (skad-(i*5)>-1&&myField[skad-(i*5)]!=0&&coJestNaPolu(skad-(i*5))==10)
           {  
               if (skad-(i*5)==D) {znalazl=true;}
               i++;
    }      }  
    i=1;
    if (kierunek==1)
    {while (skad+(i*4)<36&&myField[skad+(i*4)]!=0&&coJestNaPolu(skad+(i*4))==10)
           {  
               if (skad+(i*4)==D) {znalazl=true;}
               i++;
    }      }  
    i=1;  
    if (kierunek==3)     
    {while (skad-(i*4)>-1&&myField[skad-(i*4)]!=0&&coJestNaPolu(skad-(i*4))==10)
           {   
               if (skad-(i*4)==D) {znalazl=true;}
               i++;
    }      }                
           if (znalazl) {return true;}
           if (!znalazl) {return false;}                  
}
//==============================================================================
int cChessboard::maxBic(int kolor)
{
    int i;
    int max=0;
    for (i=0;i<36;i++)
        {   
            if (coJestNaPolu(i)>10&&kolor==1)
            {
               if (ileBic(i)>max) max=ileBic(i);
               postawWszystkie2();
            }  
            if (coJestNaPolu(i)<10&&coJestNaPolu(i)>0&&kolor==0)
            {
               if (ileBic(i)>max) max=ileBic(i);
               postawWszystkie2();
            }      
        }
   
    return max;            
} 
//==============================================================================
int cChessboard::czteryCzyPiec(int ile,int F,int D)
{   int i=1;
    int zakazane=0;
    if (abs(F-D)!=20&&ile==5) {return 5;}
    if (abs(F-D)!=20&&ile==4) {return 4;}
    if (ile==5&&F<D)
       {
           while (F+(5*i)!=D)
                 {   
                     if (F+(5*i)==31) {zakazane++;}
                     if (F+(5*i)==22) {zakazane++;}
                     if (F+(5*i)==13) {zakazane++;}
                     if (F+(5*i)==4) {zakazane++;}
                     i++;
                 
                 }
              if (i==4+zakazane) {return 5;}
              else {return 4;}           
                  
       }
       i=1; 
    zakazane=0;  
    if (ile==5&&F>D)
       {
           while (F-(5*i)!=D)
                 {   
                     if (F-(5*i)==31) {zakazane++;}
                     if (F-(5*i)==22) {zakazane++;}
                     if (F-(5*i)==13) {zakazane++;}
                     if (F-(5*i)==4) {zakazane++;}
                     i++;
                 }
           if (i==4+zakazane) {return 5;}
           else {return 4;}          
       } 
    
    
    i=1;       
    zakazane=0;  
    if (ile==4&&F>D)
       {
           while (F-(4*i)!=D)
                 {
                     if (F-(4*i)==31) {zakazane++;}
                     if (F-(4*i)==22) {zakazane++;}
                     if (F-(4*i)==13) {zakazane++;}
                     if (F-(4*i)==4) {zakazane++;}
                     i++;
                     
                 }
           if (i==5+zakazane) {return 4;}
           else {return 5;}          
       }
     i=1;     
     zakazane=0;
     if (ile==4&&F<D)
       {
           while (F+(4*i)!=D)
                 {  
                     if (F+(4*i)==31) {zakazane++;}
                     if (F+(4*i)==22) {zakazane++;}
                     if (F+(4*i)==13) {zakazane++;}
                     if (F+(4*i)==4) {zakazane++;}
                     i++;
                 }
           if (i==5+zakazane) {return 4;}
           else {return 5;}          
       }    
}    

